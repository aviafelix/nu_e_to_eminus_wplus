#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: cdivision=True

cdef double mu(double beta)
cdef double wp_nm(double x, double mu)
cdef double wp_mn(double x, double mu)
cdef double wm_nm(double x, double mu)
cdef double wm_mn(double x, double mu)
cdef double wz_nm(double x, double mu)
cdef double wz_mn(double x, double mu)
cdef double wPhi_log(double x, double mu)
cdef double wPhi_lin(double x, double mu)
cdef double wFv_log(double x, double mu)
cdef double wFv_lin(double x, double mu)
cdef double wFtau_log(double x, double mu)
cdef double wFtau_lin(double x, double mu)

cdef double K_lambda
