b = 0.1; epsilon = 1; x = 5
    wp = 9.18807e-04 GeV
    wm = 9.18807e-04 GeV
    lambda_p = 2.14764e-11 cm
    lambda_m = 2.14764e-11 cm

    wPhi = 1.85998e-03 GeV
    lambdaPhi = 1.06091e-11 cm
 =======================================
b = 0.1; epsilon = 1.5; x = 11
    wp = 7.47993e-03 GeV
    wm = 7.33803e-03 GeV
    lambda_p = 2.63809e-12 cm
    lambda_m = 2.68910e-12 cm

    wPhi = 7.90011e-03 GeV
    lambdaPhi = 2.49778e-12 cm
 =======================================
b = 0.1; epsilon = 2; x = 20
    wp = 1.58798e-02 GeV
    wm = 1.41994e-02 GeV
    lambda_p = 1.24263e-12 cm
    lambda_m = 1.38968e-12 cm

    wPhi = 1.42079e-02 GeV
    lambdaPhi = 1.38886e-12 cm
 =======================================
b = 0.1; epsilon = 2; x = 20
    wp = 1.58798e-02 GeV
    wm = 1.41994e-02 GeV
    lambda_p = 1.24263e-12 cm
    lambda_m = 1.38968e-12 cm

    wPhi = 1.42079e-02 GeV
    lambdaPhi = 1.38886e-12 cm
 =======================================
b = 0.1; epsilon = 2; x = 20
    wp = 1.58798e-02 GeV
    wm = 1.41994e-02 GeV
    lambda_p = 1.24263e-12 cm
    lambda_m = 1.38968e-12 cm

    wPhi = 1.42079e-02 GeV
    lambdaPhi = 1.38886e-12 cm
 =======================================
b = 0.5; epsilon = 1; x = 1
    wp = 2.50826e-01 GeV
    wm = 2.50826e-01 GeV
    lambda_p = 7.86710e-14 cm
    lambda_m = 7.86710e-14 cm

    wPhi = 1.89831e-01 GeV
    lambdaPhi = 1.03949e-13 cm
 =======================================
b = 0.5; epsilon = 1.5; x = 2.2
    wp = 1.55703e-01 GeV
    wm = 1.55703e-01 GeV
    lambda_p = 1.26733e-13 cm
    lambda_m = 1.26733e-13 cm

    wPhi = 3.22333e-01 GeV
    lambdaPhi = 6.12183e-14 cm
 =======================================
b = 0.5; epsilon = 2; x = 4
    wp = 2.02291e-01 GeV
    wm = 2.02291e-01 GeV
    lambda_p = 9.75462e-14 cm
    lambda_m = 9.75462e-14 cm

    wPhi = 4.43718e-01 GeV
    lambdaPhi = 4.44713e-14 cm
 =======================================
b = 0.5; epsilon = 2; x = 4
    wp = 2.02291e-01 GeV
    wm = 2.02291e-01 GeV
    lambda_p = 9.75462e-14 cm
    lambda_m = 9.75462e-14 cm

    wPhi = 4.43718e-01 GeV
    lambdaPhi = 4.44713e-14 cm
 =======================================
b = 0.5; epsilon = 2; x = 4
    wp = 2.02291e-01 GeV
    wm = 2.02291e-01 GeV
    lambda_p = 9.75462e-14 cm
    lambda_m = 9.75462e-14 cm

    wPhi = 4.43718e-01 GeV
    lambdaPhi = 4.44713e-14 cm
 =======================================
b = 1; epsilon = 1; x = 0.5
    wp = 8.27083e-01 GeV
    wm = 8.27083e-01 GeV
    lambda_p = 2.38582e-14 cm
    lambda_m = 2.38582e-14 cm

    wPhi = 8.50601e-01 GeV
    lambdaPhi = 2.31985e-14 cm
 =======================================
b = 1; epsilon = 1.5; x = 1.1
    wp = 6.27167e+00 GeV
    wm = 6.27167e+00 GeV
    lambda_p = 3.14632e-15 cm
    lambda_m = 3.14632e-15 cm

    wPhi = 1.33628e+00 GeV
    lambdaPhi = 1.47669e-14 cm
 =======================================
b = 1; epsilon = 2; x = 2
    wp = 8.30463e-01 GeV
    wm = 8.30463e-01 GeV
    lambda_p = 2.37611e-14 cm
    lambda_m = 2.37611e-14 cm

    wPhi = 1.80253e+00 GeV
    lambdaPhi = 1.09472e-14 cm
 =======================================
b = 1; epsilon = 2; x = 2
    wp = 1.01501e+00 GeV
    wm = 1.01501e+00 GeV
    lambda_p = 1.94409e-14 cm
    lambda_m = 1.94409e-14 cm

    wPhi = 1.80253e+00 GeV
    lambdaPhi = 1.09472e-14 cm
 =======================================
b = 1; epsilon = 2; x = 2
    wp = 1.01501e+00 GeV
    wm = 1.01501e+00 GeV
    lambda_p = 1.94409e-14 cm
    lambda_m = 1.94409e-14 cm

    wPhi = 1.80253e+00 GeV
    lambdaPhi = 1.09472e-14 cm
 =======================================
b = 1; epsilon = 1; x = 0.5
    wp = 8.27083e-01 GeV
    wm = 8.27083e-01 GeV
    lambda_p = 2.38582e-14 cm
    lambda_m = 2.38582e-14 cm

    wPhi = 8.50601e-01 GeV
    lambdaPhi = 2.31985e-14 cm
 =======================================
b = 1; epsilon = 1.5; x = 1.1
    wp = 6.27167e+00 GeV
    wm = 6.27167e+00 GeV
    lambda_p = 3.14632e-15 cm
    lambda_m = 3.14632e-15 cm

    wPhi = 1.33628e+00 GeV
    lambdaPhi = 1.47669e-14 cm
 =======================================
b = 1; epsilon = 2; x = 2
    wp = 8.30463e-01 GeV
    wm = 8.30463e-01 GeV
    lambda_p = 2.37611e-14 cm
    lambda_m = 2.37611e-14 cm

    wPhi = 1.80253e+00 GeV
    lambdaPhi = 1.09472e-14 cm
 =======================================
b = 1; epsilon = 2; x = 2
    wp = 1.01501e+00 GeV
    wm = 1.01501e+00 GeV
    lambda_p = 1.94409e-14 cm
    lambda_m = 1.94409e-14 cm

    wPhi = 1.80253e+00 GeV
    lambdaPhi = 1.09472e-14 cm
 =======================================
b = 1; epsilon = 2; x = 2
    wp = 1.01501e+00 GeV
    wm = 1.01501e+00 GeV
    lambda_p = 1.94409e-14 cm
    lambda_m = 1.94409e-14 cm

    wPhi = 1.80253e+00 GeV
    lambdaPhi = 1.09472e-14 cm
 =======================================
b = 1; epsilon = 1; x = 0.5
    wp = 8.27083e-01 GeV
    wm = 8.27083e-01 GeV
    lambda_p = 2.38582e-14 cm
    lambda_m = 2.38582e-14 cm

    wPhi = 8.50601e-01 GeV
    lambdaPhi = 2.31985e-14 cm
 =======================================
b = 1; epsilon = 1.5; x = 1.1
    wp = 6.27167e+00 GeV
    wm = 6.27167e+00 GeV
    lambda_p = 3.14632e-15 cm
    lambda_m = 3.14632e-15 cm

    wPhi = 1.33628e+00 GeV
    lambdaPhi = 1.47669e-14 cm
 =======================================
b = 1; epsilon = 2; x = 2
    wp = 8.30463e-01 GeV
    wm = 8.30463e-01 GeV
    lambda_p = 2.37611e-14 cm
    lambda_m = 2.37611e-14 cm

    wPhi = 1.80253e+00 GeV
    lambdaPhi = 1.09472e-14 cm
 =======================================
b = 1; epsilon = 2; x = 2
    wp = nan GeV
    wm = nan GeV
    lambda_p = nan cm
    lambda_m = nan cm

    wPhi = 1.80253e+00 GeV
    lambdaPhi = 1.09472e-14 cm
 =======================================
b = 1; epsilon = 2; x = 2
    wp = 3.69062e+11 GeV
    wm = 3.69062e+11 GeV
    lambda_p = 5.34672e-26 cm
    lambda_m = 5.34672e-26 cm

    wPhi = 1.80253e+00 GeV
    lambdaPhi = 1.09472e-14 cm
 =======================================
