Замечания по формуле:
=====================
 * Нужно брать не целую часть ("[]" или "Integer"), а округлять вниз до целого числа. Это важно в численных расчётах (в предположении, что при верхнем пределе суммирования, меньшем нижнего, сумма будет равна нулю, то есть не будет включать "нулевые слагаемые" -- слагаемые при равных нулю индексах). И это можно предположить при математической записи суммы. Хотя тэта-функция должна, кажется, отсекать такие слагаемые.
 * Знак в формуле у слагаемого, не сводящегося к квадрату?

Общие мысли:
============
 * Подумать об итерациях по "input_data" в plotter.py или организовать структуру более подходящим образом
 * Сделать описание того, что здесь происходит
 * Упростить написанный код, структурировать его


About cythonizing:
==================
* [http://cython-docs2.readthedocs.io/en/latest/src/tutorial/numpy.html](http://cython-docs2.readthedocs.io/en/latest/src/tutorial/numpy.html)
* [http://cython.readthedocs.io/en/latest/src/tutorial/numpy.html](http://cython.readthedocs.io/en/latest/src/tutorial/numpy.html)
* [http://cython.readthedocs.io/en/latest/src/userguide/numpy_tutorial.html](http://cython.readthedocs.io/en/latest/src/userguide/numpy_tutorial.html)
* [https://python.g-node.org/python-summerschool-2011/_media/materials/cython/cython-slides.pdf](https://python.g-node.org/python-summerschool-2011/_media/materials/cython/cython-slides.pdf)
* [https://github.com/fredRos/pypmc/wiki/Converting-a-python-module-with-cython](https://github.com/fredRos/pypmc/wiki/Converting-a-python-module-with-cython)

 * **Isbjörn**
 * **Isbjørn**
