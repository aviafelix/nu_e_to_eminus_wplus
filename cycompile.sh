#!/bin/sh
# 
LOGFILE=_cythonize.log
[[ -f $LOGFILE ]] && rm $LOGFILE || echo "no log file"

cythonize() {
    echo "Cythonizing pyx module \`$1\`..."

    {
        [[ -f $1.so ]] && rm $1.so
        # for python2:
        # cython3 -o $1.c $1.pyx && \
        # for python3:
        cython3 -3 -o $1.c $1.pyx && \
        gcc -shared -pthread \
            -fPIC -fwrapv -O3 -Wall \
            -fno-strict-aliasing \
            -I/home/okrugin/.local/include \
            -I/home/okrugin/.local/include/python3.5m \
            -lpython3.5m -lpthread -lm -lutil -ldl \
            -o $1.so $1.c
        _ecode=$?
        [[ -f $1.c ]] && rm $1.c
    } 1>> $LOGFILE 2>&1

    [[ "$_ecode" = "0" ]] && echo "Ok, cythonized! :o)" || { echo -e " [-] _NOT_ CYTHONIZED [-] :O(\n"; cat $LOGFILE; exit 1; }
}
# 
cythonize_py() {
    echo "Cythonizing py module \`$1\`..."

    {
        [[ -f $1.so ]] && rm $1.so
        # for python2:
        # cython3 -o $1.c $1.py && \
        # for python3:
        cython3 -3 -o $1.c $1.py && \
        gcc -shared -pthread \
            -fPIC -fwrapv -O3 -Wall \
            -fno-strict-aliasing \
            -I/home/okrugin/.local/include \
            -I/home/okrugin/.local/include/python3.5m \
            -lpython3.5m -lpthread -lm -lutil -ldl \
            -o $1.so $1.c
        _ecode=$?
        [[ -f $1.c ]] && rm $1.c
    } 1>> $LOGFILE 2>&1

    [[ "$_ecode" = "0" ]] && echo "Ok, cythonized! :o)" || { echo -e " [-] _NOT_ CYTHONIZED [-] :O(\n"; cat $LOGFILE; exit 1; }
}
# 
# cython wnuetowpluseminus.pyx
# gcc -shared -pthread -fPIC -fwrapv -O3 -Wall -fno-strict-aliasing -I/home/okrugin/.local/include/python3.5m -o wnuetowpluseminus.so wnuetowpluseminus.c
# 
# cython --embed wnuetowpluseminus.pyx
# gcc -pthread -fPIC -Os -O3 -Wall -fno-strict-aliasing -I/home/okrugin/.local/include/python3.5m -o wnuetowpluseminus wnuetowpluseminus.c -l/home/okrugin/.local/lib/python3.5m -lpthread -lm -lutil -ldl
# 
# nuitka --recurse-all wnuetowpluseminus.pyx
# nuitka --recurse-none wnuetowpluseminus.pyx
# nuitka --standalone --recurse-all --recurse-stdlib wnuetowpluseminus.pyx
# 
#
cythonize _calcfunctions
cythonize _wcalculate
#
# cythonize_py plotter
ln -s plotter.py _plotter.pyx
cythonize _plotter
rm _plotter.pyx
# 
# cythonize tests/_ctest
