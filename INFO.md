[Table of Fundamental Physical Constants](http://physics.nist.gov/cuu/Constants/Table/allascii.txt) (From:  [http://physics.nist.gov/constants](http://physics.nist.gov/constants))

  * ![$\hbar$ = 6.582119514 10^-16 eV s](//tex.s2cms.ru/svg/%24%5Chbar%20%3D%206.582119514%20%5Ccdot%2010%5E%7B-16%7D%5C%2C%5Cmathrm%7BeV%7D%5Ccdot%5Cmathrm%7Bs%7D%24)
  * ![$c$ = 2.99792458 10^10 cm s](//tex.s2cms.ru/svg/%24c%20%3D%202.99792458%5Ccdot%2010%5E%7B10%7D%5C%2C%5Cmathrm%7Bcm%7D%5Ccdot%5Cmathrm%7Bs%7D%24)
  * ![$\hbar \cdot c$ = 1.97326979 10^-14 GeV cm](//tex.s2cms.ru/svg/%24%5Chbar%5C%2Cc%20%3D%201.97326979%5Ccdot10%5E%7B-14%7D%5C%2C%5Cmathrm%7BGeV%7D%5Ccdot%5Cmathrm%7Bcm%7D%24)
