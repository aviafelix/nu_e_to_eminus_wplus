import math
import scipy.special as ss

# http://physics.nist.gov/cuu/Constants/Table/allascii.txt
m_e = 0.5109989461E-03 # electron mass in GeV/c^2
m_W = 80.385          # W-boson mass in GeV/c^2
G_F = 1.16637E-05     # Fermi coupling constant in GeV^{-2}
Kfp = G_F * m_W * m_W * m_W / (2 * math.sqrt(2) * math.pi)        # [GeV] the common factor of the probability
KPhi = G_F * m_W * m_W * m_W / (8 * math.pi * math.sqrt(math.pi)) # [GeV] the common factor of the probability
                                                                  # from the formula with integral 1
KFv = G_F * m_W * m_W * m_W * math.sqrt(2.0/3.0) / \
      (12.0 * math.pi * math.pi)                                  # [GeV] the common factor of the probability
                                                                  # from the formula with integral 2
KFtau = G_F * m_W * m_e * m_e * math.sqrt(2.0/3.0) / \
      (12.0 * math.pi * math.pi)                                  # [GeV] the common factor of the probability
                                                                  # from the formula with integral 2
K_lambda = 6.582119514E-16 * 2.99792458E+01                       # [GeV cm] the factor to convert GeV^{-1} to cm
_lambda = (m_e*m_e) / (m_W*m_W)
b = (m_W*m_W) / (m_e*m_e)

floor = math.floor
trunc = math.trunc
sqrt = math.sqrt
exp = math.exp
cosh = math.cosh
sinh = math.sinh
tanh = math.tanh
copysign = math.copysign
sign = lambda x: copysign(1.0, x)
isfinite = math.isfinite

laguerre_l = ss.eval_genlaguerre
bessel_k = ss.kv


def mu(beta):
    """
    m_W^2 / (2 \beta);
    here beta is dimensionless parameter in units of m_W^2.
    """
    return 0.5 / beta


def n_max(x, mu, m=0):
    """
    n_max = (\sqrt{x} - \sqrt{m})^2 - \mu + 1/2
    """
    if m == 0:
        return floor(x-mu+0.5)
    else:
        t = sqrt(x) - sqrt(m)
        return floor(t*t - mu + 0.5)


def m_max(x, mu, n=0):
    """
    m_max = (\sqrt{x} - \sqrt{n + \mu - 1/2})^2
    """
    t = sqrt(x) - sqrt(n+mu-0.5)
    return floor(t*t)


def d(x, mu, n, m):
    """
    $d = \sqrt{ \left(x - \mu - n + m + 1/2\right)^2 - 4 \, m \, x };$
    """
    a = mu + n - 0.5

    if (m == 0):
        return x - a

    if (a == 0.0):
        return x - m

    t = x + m - a

    # `abs` is for small negative values
    return sqrt(abs(t*t - 4*m*x))
    # return sqrt(t*t - 4*m*x)


def heaviside_theta(x):
    """
    Def. of Heaviside theta function
    """
    return 0.5 * (copysign(1.0, x) + 1.0)

theta = heaviside_theta


def pow_multifactorial(x, a, b):
    """
    """
    if a == b:
        return 1.0
    if a < 0:
        if b < 0:
            return float('nan')
        else:
            return float('inf')
    elif b < 0:
        return 0.0

    s = 1.0
    if a >= b:
        for i in range(b+1, a+1):
            s *= i/x
    else:
        for i in range(a+1, b+1):
            s *= x/i
    return s


def wp_nm(x, mu):
    """
    """
    s = 0.0

    nmax = n_max(x, mu)
    for n in range(nmax+1):
        mmax = m_max(x, mu, n)
        for m in range(mmax+1):
            if  (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p0 = pow_multifactorial(x, n-1, m-1)
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(n-2, m-n+1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(n, m-n, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(n-1, m-n+1, x) * laguerre_l(n-1, m-n, x)
                    if l0 != 0.0:
                        l0 = 4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
                else:
                    # n > m
                    p0 = pow_multifactorial(x, m, n-1)
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(m-1, n-m-1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(m, n-m, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(m, n-m-1, x) * laguerre_l(m-1, n-m, x)
                    if l0 != 0.0:
                        l0 = -4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


def wp_mn(x, mu):
    """
    """
    s = 0.0

    mmax = m_max(x, mu)
    for m in range(mmax+1):
        nmax = n_max(x, mu, m)
        for n in range(nmax+1):
            if (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p0 = pow_multifactorial(x, n-1, m-1)
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(n-2, m-n+1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(n, m-n, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(n-1, m-n+1, x) * laguerre_l(n-1, m-n, x)
                    if l0 != 0.0:
                        l0 = 4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
                else:
                    # n > m
                    p0 = pow_multifactorial(x, m, n-1)
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(m-1, n-m-1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(m, n-m, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(m, n-m-1, x) * laguerre_l(m-1, n-m, x)
                    if l0 != 0.0:
                        l0 = -4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


def wm_nm(x, mu):
    """
    """
    s = 0.0

    nmax = n_max(x, mu)
    for n in range(nmax+1):
        mmax = m_max(x, mu, n)
        for m in range(mmax+1):
            if  (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p0 = pow_multifactorial(x, n-1, m-1)
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(n-2, m-n+1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(n, m-n, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(n-1, m-n+1, x) * laguerre_l(n-1, m-n, x)
                    if l0 != 0.0:
                        l0 = -4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
                else:
                    # n > m
                    p0 = pow_multifactorial(x, m, n-1)
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(m-1, n-m-1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(m, n-m, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(m, n-m-1, x) * laguerre_l(m-1, n-m, x)
                    if l0 != 0.0:
                        l0 = 4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


def wm_mn(x, mu):
    """
    """
    s = 0.0

    mmax = m_max(x, mu)
    for m in range(mmax+1):
        nmax = n_max(x, mu, m)
        for n in range(nmax+1):
            if (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p0 = pow_multifactorial(x, n-1, m-1)
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(n-2, m-n+1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(n, m-n, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(n-1, m-n+1, x) * laguerre_l(n-1, m-n, x)
                    if l0 != 0.0:
                        l0 = -4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
                else:
                    # n > m
                    p0 = pow_multifactorial(x, m, n-1)
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(m-1, n-m-1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(m, n-m, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(m, n-m-1, x) * laguerre_l(m-1, n-m, x)
                    if l0 != 0.0:
                        l0 = 4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


def wz_nm(x, mu):
    """
    """
    s = 0.0

    nmax = n_max(x, mu)
    for n in range(nmax+1):
        mmax = m_max(x, mu, n)
        for m in range(mmax+1):
            if  (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(n-2, m-n+1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(n, m-n, x)
                    l1 = p1 * l1 * l1

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += (x - mu - n + m + 0.5) * (l1 + l2) / d_x_mu_n_m
                else:
                    # n > m
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(m-1, n-m-1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(m, n-m, x)
                    l1 = p1 * l1 * l1

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += (x - mu - n + m + 0.5) * (l1 + l2) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


def wz_mn(x, mu):
    """
    """
    s = 0.0

    mmax = m_max(x, mu)
    for m in range(mmax+1):
        nmax = n_max(x, mu, m)
        for n in range(nmax+1):
            if (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(n-2, m-n+1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(n, m-n, x)
                    l1 = p1 * l1 * l1

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += (x - mu - n + m + 0.5) * (l1 + l2) / d_x_mu_n_m
                else:
                    # n > m
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(m-1, n-m-1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(m, n-m, x)
                    l1 = p1 * l1 * l1

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += (x - mu - n + m + 0.5) * (l1 + l2) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


def bounds_gen(x_max, mu):
    """
    b = (sqrt{m} + \sqrt{n + \mu - 1/2})^2 = m + n + \mu - 1/2 + 2 \sqrt{m (n + \mu - 1/2)} @ x >= \mu - 1/2
    """
    nmax = n_max(x_max, mu)
    for n in range(nmax+1):
        mmax = m_max(x_max, mu, n)
        for m in range(mmax+1):
            x = n + mu - 0.5
            # if m != 0:
            x = m + x + 2.0 * sqrt(m * x)
            if x >= mu - 0.5:
                yield x
            else:
                continue
    yield x


def rl_nm(x, mu):
    """
    the ratio of all three summands of the sum `wm_nm(x, mu)`
    """
    s, sl0, sl1, sl2, = 0.0, 0.0, 0.0, 0.0,

    nmax = n_max(x, mu)
    for n in range(nmax+1):
        mmax = m_max(x, mu, n)
        for m in range(mmax+1):
            if  (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p0 = pow_multifactorial(x, n-1, m-1)
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(n-2, m-n+1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(n, m-n, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(n-1, m-n+1, x) * laguerre_l(n-1, m-n, x)
                    if l0 != 0.0:
                        l0 = 4.0 * p0 * x * l0 # orig

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    sl0 += l0 / d_x_mu_n_m
                    sl1 += (x - mu - n + m + 0.5) * l1 / d_x_mu_n_m
                    sl2 += (x - mu - n + m + 0.5) * l2 / d_x_mu_n_m
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
                else:
                    # n > m
                    p0 = pow_multifactorial(x, m, n-1)
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(m-1, n-m-1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(m, n-m, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(m, n-m-1, x) * laguerre_l(m-1, n-m, x)
                    if l0 != 0.0:
                        l0 = -4.0 * p0 * x * l0 # orig

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    sl0 += l0 / d_x_mu_n_m
                    sl1 += (x - mu - n + m + 0.5) * l1 / d_x_mu_n_m
                    sl2 += (x - mu - n + m + 0.5) * l2 / d_x_mu_n_m
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m

    return (sl0/s, sl1/s, sl2/s, )


def rl_mn(x, mu):
    """
    the ratio of all three summands of the sum `wm_mn(x, mu)`
    """
    s, sl0, sl1, sl2, = 0.0, 0.0, 0.0, 0.0,

    mmax = m_max(x, mu)
    for m in range(mmax+1):
        nmax = n_max(x, mu, m)
        for n in range(nmax+1):
            if (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p0 = pow_multifactorial(x, n-1, m-1)
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(n-2, m-n+1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(n, m-n, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(n-1, m-n+1, x) * laguerre_l(n-1, m-n, x)
                    if l0 != 0.0:
                        l0 = 4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    sl0 += l0 / d_x_mu_n_m
                    sl1 += (x - mu - n + m + 0.5) * l1 / d_x_mu_n_m
                    sl2 += (x - mu - n + m + 0.5) * l2 / d_x_mu_n_m
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
                else:
                    # n > m
                    p0 = pow_multifactorial(x, m, n-1)
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = laguerre_l(m-1, n-m-1, x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = laguerre_l(m, n-m, x)
                    l1 = p1 * l1 * l1

                    l0 = laguerre_l(m, n-m-1, x) * laguerre_l(m-1, n-m, x)
                    if l0 != 0.0:
                        l0 = -4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    sl0 += l0 / d_x_mu_n_m
                    sl1 += (x - mu - n + m + 0.5) * l1 / d_x_mu_n_m
                    sl2 += (x - mu - n + m + 0.5) * l2 / d_x_mu_n_m
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
    
    return (sl0/s, sl1/s, sl2/s, )


def gen_wp_nm(mu):
    """
    returns closure for wp_nm(x, mu=mu)
    """
    # from calcfunctions import wp_nm
    wp_nm.__defaults__ = (mu, )
    return wp_nm


def gen_wp_mn(mu):
    """
    returns closure for wp_mn(x, mu=mu)
    """
    # from calcfunctions import wp_nm
    wp_mn.__defaults__ = (mu, )
    return wp_mn


def gen_wm_nm(mu):
    """
    returns closure for wm_nm(x, mu=mu)
    """
    # from calcfunctions import wm_nm
    wm_nm.__defaults__ = (mu, )
    return wm_nm


def gen_wm_mn(mu):
    """
    returns closure for wm_mn(x, mu=mu)
    """
    # from calcfunctions import wm_nm
    wm_mn.__defaults__ = (mu, )
    return wm_mn


def phi(y, eta):
    """
    auxiliary function to integrate for wPhi_lin(x, mu) and wPhi_log(x, mu)
    """
    if y == 0.0:
        return 0.0

    thy = tanh(y)
    y_m_thy = y - thy
    y_thy = y * thy

    x = exp(-2.0 * y)
    z = x + (1.0 + x) * y - 1.0 # (1+x) * y - (1-x)

    return (1.0 - x*x - 4.0*x*y) / (z * sqrt(y * z * (1.0-x))) * exp(-y_thy / (eta * y_m_thy))


def gen_phi(eta):
    """
    returns closure for phi(y, eta=eta)
    """
    # from calcfunctions import phi
    phi.__defaults__ = (eta, )
    return phi


def wPhi_lin(x, mu):
    """
    """
    eta = 2.0 * x / (mu*mu)
    phi = gen_phi(eta)

    # def phi(y, eta=eta):
    #     """
    #     auxiliary function to integrate for wPhi_lin(x, mu) and wPhi_log(x, mu).
    #     it works with definition inside this function as fast as closure for it
    #     that is generated by the gen_phi(eta) function
    #     """
    #     if y == 0.0:
    #         return 0.0

    #     thy = tanh(y)
    #     y_m_thy = y - thy
    #     y_thy = y * thy

    #     x = exp(-2.0 * y)
    #     z = x + (1.0 + x) * y - 1.0 # (1+x) * y - (1-x)

    #     return (1.0 - x*x - 4.0*x*y) / (z * sqrt(y * z * (1.0-x))) * exp(-y_thy / (eta * y_m_thy))

    return KPhi * sqrt(mu) / x * integrate(phi, 0.0E+00, 1.0E+05, n=320000)


def wPhi_log(x, mu):
    """
    """
    eta = 2.0 * x / (mu*mu)
    phi = gen_phi(eta)

    # def phi(y, eta=eta):
    #     """
    #     auxiliary function to integrate for wPhi_lin(x, mu) and wPhi_log(x, mu).
    #     it works with definition inside this function as fast as closure for it
    #     that is generated by the gen_phi(eta) function
    #     """
    #     if y == 0.0:
    #         return 0.0

    #     thy = tanh(y)
    #     y_m_thy = y - thy
    #     y_thy = y * thy

    #     x = exp(-2.0 * y)
    #     z = x + (1.0 + x) * y - 1.0 # (1+x) * y - (1-x)

    #     return (1.0 - x*x - 4.0*x*y) / (z * sqrt(y * z * (1.0-x))) * exp(-y_thy / (eta * y_m_thy))

    return KPhi * sqrt(mu) / x * logintegrate(phi, 1.0E-05, 1.0E+05, n=64000)


def Fv(v, chi, _lambda):
    """
    auxiliary function to integrate for wFv_lin(x, mu) and wFv_log(x, mu) [1]
    """
    one_minus_v = 1.0 - v
    v_p_lambda_1mv = v + _lambda * one_minus_v
    u = 2.0 * v_p_lambda_1mv * sqrt(v_p_lambda_1mv) / (3.0 * chi * v * one_minus_v)

    return (
        v_p_lambda_1mv * (2.0 * (1.0 + v) * (2.0 + v) + _lambda * one_minus_v * (2.0 - v))
    ) * bessel_k(2.0/3.0, u) / (v * one_minus_v * one_minus_v)


def gen_Fv(chi, _lambda):
    """
    returns closure for Fv(tau, chi=chi, _lambda=_lambda)
    """
    Fv.__defaults__ = (chi, _lambda, )
    return Fv


def wFv_lin(x, mu):
    """
    """
    chi = sqrt(x) / (2.0 * mu * sqrt(mu))
    Fv = gen_Fv(chi, _lambda)

    # def Fv(v, chi=chi, _lambda=_lambda):
    #     """
    #     """
    #     one_minus_v = 1.0 - v
    #     v_p_lambda_1mv = v + _lambda * one_minus_v
    #     u = 2.0 * v_p_lambda_1mv * sqrt(v_p_lambda_1mv) / (3.0 * chi * v * one_minus_v)

    #     return (
    #         v_p_lambda_1mv * (2.0 * (1.0 + v) * (2.0 + v) + _lambda * one_minus_v * (2.0 - v))
    #     ) * bessel_k(2.0/3.0, u) / (v * one_minus_v * one_minus_v)

    return KFv * sqrt(mu/x) * integrate(Fv, 1.0E-20, 1.0-1.0E-12, n=320000)


def wFv_log(x, mu):
    """
    """
    chi = sqrt(x) / (2.0 * mu * sqrt(mu))
    Fv = gen_Fv(chi, _lambda)

    # def Fv(v, chi=chi, _lambda=_lambda):
    #     """
    #     """
    #     one_minus_v = 1.0 - v
    #     v_p_lambda_1mv = v + _lambda * one_minus_v
    #     u = 2.0 * v_p_lambda_1mv * sqrt(v_p_lambda_1mv) / (3.0 * chi * v * one_minus_v)

    #     return (
    #         v_p_lambda_1mv * (2.0 * (1.0 + v) * (2.0 + v) + _lambda * one_minus_v * (2.0 - v))
    #     ) * bessel_k(2.0/3.0, u) / (v * one_minus_v * one_minus_v)

    return KFv * sqrt(mu/x) * logintegrate(Fv, 1.0E-20, 1.0-1.0E-12, n=65536)


def Ftau(tau, chi, b):
    """
    auxiliary function to integrate for wFtau_lin(x, mu) and wFtau_log(x, mu) [2]
    """
    if tau == 0.0:
        return 0.0

    tau_plus_b = tau + b
    tau_plus_1 = tau + 1.0
    u = 2.0 * sqrt(tau_plus_b*tau_plus_1) * tau_plus_1 / (3.0 * chi * tau * b)

    return (
        tau_plus_1 * (2.0 * (2.0*tau + b) * (3.0*tau + 2.0*b) + (tau + 2.0*b))
    ) * bessel_k(2.0/3.0, u) / (tau * tau_plus_b * tau_plus_b)


def gen_Ftau(chi, b):
    """
    returns closure for Ftau(tau, chi=chi, b=b)
    """
    Ftau.__defaults__ = (chi, b, )
    return Ftau


def wFtau_lin(x, mu):
    """
    """
    chi = sqrt(x) / (2.0 * mu * sqrt(mu))
    Ftau = gen_Ftau(chi, b)

    # def Ftau(tau, chi=chi, b=b):
    #     """
    #     auxiliary function to integrate for wFtau_lin(x, mu) and wFtau_log(x, mu)
    #     """
    #     if tau == 0.0:
    #         return 0.0

    #     tau_plus_b = tau + b
    #     tau_plus_1 = tau + 1.0
    #     u = 2.0 * _lambda * sqrt(tau_plus_b*tau_plus_1) * tau_plus_1 / (3.0 * chi * tau)

    #     return (
    #         tau_plus_1 * (2.0 * (2.0*tau + b) * (3.0*tau + 2.0*b) + (tau + 2.0*b))
    #     ) * bessel_k(2.0/3.0, u) / (tau * tau_plus_b * tau_plus_b)

    return KFtau * sqrt(mu/x) * integrate(Ftau, 1.0E-16, 1.0E+03*b, n=320000)


def wFtau_log(x, mu):
    """
    """
    chi = sqrt(x) / (2.0 * mu * sqrt(mu))
    Ftau = gen_Ftau(chi, b)

    # def Ftau(tau, chi=chi, b=b):
    #     """
    #     auxiliary function to integrate for wFtau_lin(x, mu) and wFtau_log(x, mu)
    #     """
    #     if tau == 0.0:
    #         return 0.0

    #     tau_plus_b = tau + b
    #     tau_plus_1 = tau + 1.0
    #     u = 2.0 * _lambda * sqrt(tau_plus_b*tau_plus_1) * tau_plus_1 / (3.0 * chi * tau)

    #     return (
    #         tau_plus_1 * (2.0 * (2.0*tau + b) * (3.0*tau + 2.0*b) + (tau + 2.0*b))
    #     ) * bessel_k(2.0/3.0, u) / (tau * tau_plus_b * tau_plus_b)

    return KFtau * sqrt(mu/x) * logintegrate(Ftau, 1.0E-16, 1.0E+03*b, n=65536)


def integrate(f, minvalue, maxvalue, n=32000):
    '''
    Integration f over x in linear scale.
    Values are summarized in n points.
    '''
    delta = maxvalue - minvalue
    dx = delta / n

    S = 0.0

    # Trapeze
    x_curr = minvalue

    for i in range(n):
        x_prev = x_curr
        x_curr = minvalue + (i+1.0) * dx
        x = minvalue + (i+0.5) * dx
        S += (0.25 * (f(x_prev) + f(x_curr)) + 0.5*f(x)) * dx

    return S


def logintegrate(f, minvalue, maxvalue, n=32000):
    '''
    Integration f over x in logarithmic scale.
    Values are summarized in n points.
    '''
    exp = math.exp

    xi_min = math.log(minvalue)
    xi_max = math.log(maxvalue)

    delta_xi = xi_max - xi_min
    d_xi = delta_xi / n

    S = 0.0

    # Trapeze
    xi_curr = xi_min

    for i in range(n):
        xi_prev = xi_curr
        xi_curr = xi_min + (i+1.0) * d_xi
        xi = xi_min + (i+0.5) * d_xi

        S += (
            0.25 * (
                f(exp(xi_prev)) * exp(xi_prev) +
                f(exp(xi_curr)) * exp(xi_curr)
            )
            + 0.5 * f(exp(xi)) * exp(xi)
        ) * d_xi

    return S
