#!/usr/bin/env cython
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: cdivision=True
# 
# cimport cython
from _calcfunctions cimport *
from scipy.special.cython_special cimport kv, eval_genlaguerre
# pi and e constants will be in the next version of cython (>0.24.1):
from libc.math cimport sqrt, M_PI, floor, trunc, exp, log, cosh, sinh, tanh, isfinite, copysign, NAN, INFINITY
# from libc.math cimport sqrt, pi, floor, trunc, exp, log, cosh, sinh, tanh, isfinite, copysign, NAN, INFINITY
# 
cdef double pi = M_PI
# 
# # http://physics.nist.gov/cuu/Constants/Table/allascii.txt
# cdef double m_e = 0.5109989461E-03 # electron mass in GeV/c^2
# cdef double m_W = 80.385          # W-boson mass in GeV/c^2
# cdef double G_F = 1.16637E-05     # Fermi coupling constant in GeV^{-2}
# cdef double K_lambda = 6.582119514E-16 * 2.99792458E+01        # [GeV cm] the factor to convert GeV^{-1} to cm
# cdef double _lambda = (m_e*m_e) / (m_W*m_W)
# cdef double b = (m_W*m_W) / (m_e*m_e)
#
# http://physics.nist.gov/cuu/Constants/Table/allascii.txt
m_e = 0.5109989461E-03 # electron mass in GeV/c^2
m_W = 80.385          # W-boson mass in GeV/c^2
G_F = 1.16637E-05     # Fermi coupling constant in GeV^{-2}
K_lambda = 6.582119514E-16 * 2.99792458E+01        # [GeV cm] the factor to convert GeV^{-1} to cm
_lambda = (m_e*m_e) / (m_W*m_W)
b = (m_W*m_W) / (m_e*m_e)
#
cdef double Kfp = G_F * m_W * m_W * m_W / (2 * sqrt(2) * pi)   # [GeV] the common factor of the probability
cdef double KPhi = G_F * m_W * m_W * m_W / (8 * pi * sqrt(pi)) # [GeV] the common factor of the probability
                                                               # from the formula with integral 1
cdef double KFv = G_F * m_W * m_W * m_W * sqrt(2.0/3.0) / \
      (12.0 * pi * pi)                                         # [GeV] the common factor of the probability
                                                               # from the formula with integral 2
cdef double KFtau = G_F * m_W * m_e * m_e * sqrt(2.0/3.0) / \
      (12.0 * pi * pi)                                         # [GeV] the common factor of the probability
                                                               # from the formula with integral 2

# sign = lambda x: copysign(1.0, x)
cdef inline double sign(double x): return copysign(1.0, x)


cdef double mu(double beta):
    """
    m_W^2 / (2 \beta);
    here beta is dimensionless parameter in units of m_W^2.
    """
    return 0.5 / beta


cdef signed long n_max(double x, double mu, signed long m=0):
    """
    n_max = (\sqrt{x} - \sqrt{m})^2 - \mu + 1/2
    """
    cdef double t

    if m == 0:
        return <signed long>floor(x-mu+0.5)
    else:
        t = sqrt(x) - sqrt(<double>m)
        return <signed long>floor(t*t - mu + 0.5)


cdef signed long m_max(double x, double mu, signed long n=0):
    """
    m_max = (\sqrt{x} - \sqrt{n + \mu - 1/2})^2
    """
    cdef double t

    t = sqrt(x) - sqrt(<double>n+mu-0.5)
    return <signed long>floor(t*t)


cdef double d(double x, double mu, signed long n, signed long m):
    """
    $d = \sqrt{ \left(x - \mu - n + m + 1/2\right)^2 - 4 \, m \, x };$
    """
    cdef double a = mu + <double>n - 0.5

    if (m == 0):
        return x - a

    if (a == 0.0):
        return x - <double>m

    t = x + <double>m - a

    # `abs` is for small negative values
    return sqrt(abs(t*t - 4*m*x))
    # return sqrt(t*t - 4*m*x)


cdef double heaviside_theta(double x):
    """
    Def. of Heaviside theta function
    """
    return 0.5 * (copysign(1.0, x) + 1.0)

theta = heaviside_theta


cdef double pow_multifactorial(double x, signed long a, signed long b):
    """
    """
    cdef size_t i

    if a == b:
        return 1.0
    if a < 0:
        if b < 0:
            return NAN
        else:
            return INFINITY
    elif b < 0:
        return 0.0

    cdef double s = 1.0

    if a >= b:
        for i in range(b+1, a+1):
            s *= <double>i/x
    else:
        for i in range(a+1, b+1):
            s *= x/<double>i
    return s


cdef double wp_nm(double x, double mu):
    """
    """
    cdef double p0, p1, p2, l0, l1, l2, d_x_mu_n_m
    cdef signed long m, n
    cdef signed long nmax, mmax
    cdef double s = 0.0

    nmax = n_max(x, mu)
    for n in range(nmax+1):
        mmax = m_max(x, mu, n)
        for m in range(mmax+1):
            if  (sqrt(x) - sqrt(<double>n + mu - 0.5) - sqrt(<double>m) >= 0.0):
                if m >= n:
                    p0 = pow_multifactorial(x, n-1, m-1)
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(n-2, <double>(m-n+1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(n, <double>(m-n), x)
                    l1 = p1 * l1 * l1

                    l0 = eval_genlaguerre(n-1, <double>(m-n+1), x) * eval_genlaguerre(n-1, <double>(m-n), x)
                    if l0 != 0.0:
                        l0 = 4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
                else:
                    # n > m
                    p0 = pow_multifactorial(x, m, n-1)
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(m-1, <double>(n-m-1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(m, <double>(n-m), x)
                    l1 = p1 * l1 * l1

                    l0 = eval_genlaguerre(m, <double>(n-m-1), x) * eval_genlaguerre(m-1, <double>(n-m), x)
                    if l0 != 0.0:
                        l0 = -4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


cdef double wp_mn(double x, double mu):
    """
    """
    cdef double p0, p1, p2, l0, l1, l2, d_x_mu_n_m
    cdef signed long m, n
    cdef signed long nmax, mmax
    cdef double s = 0.0

    mmax = m_max(x, mu)
    for m in range(mmax+1):
        nmax = n_max(x, mu, m)
        for n in range(nmax+1):
            if (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p0 = pow_multifactorial(x, n-1, m-1)
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(n-2, <double>(m-n+1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(n, <double>(m-n), x)
                    l1 = p1 * l1 * l1

                    l0 = eval_genlaguerre(n-1, <double>(m-n+1), x) * eval_genlaguerre(n-1, <double>(m-n), x)
                    if l0 != 0.0:
                        l0 = 4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
                else:
                    # n > m
                    p0 = pow_multifactorial(x, m, n-1)
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(m-1, <double>(n-m-1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(m, <double>(n-m), x)
                    l1 = p1 * l1 * l1

                    l0 = eval_genlaguerre(m, <double>(n-m-1), x) * eval_genlaguerre(m-1, <double>(n-m), x)
                    if l0 != 0.0:
                        l0 = -4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


cdef double wm_nm(double x, double mu):
    """
    """
    cdef double p0, p1, p2, l0, l1, l2, d_x_mu_n_m
    cdef signed long m, n
    cdef signed long nmax, mmax
    cdef double s = 0.0

    nmax = n_max(x, mu)
    for n in range(nmax+1):
        mmax = m_max(x, mu, n)
        for m in range(mmax+1):
            if  (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p0 = pow_multifactorial(x, n-1, m-1)
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(n-2, <double>(m-n+1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(n, <double>(m-n), x)
                    l1 = p1 * l1 * l1

                    l0 = eval_genlaguerre(n-1, <double>(m-n+1), x) * eval_genlaguerre(n-1, <double>(m-n), x)
                    if l0 != 0.0:
                        l0 = -4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
                else:
                    # n > m
                    p0 = pow_multifactorial(x, m, n-1)
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(m-1, <double>(n-m-1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(m, <double>(n-m), x)
                    l1 = p1 * l1 * l1

                    l0 = eval_genlaguerre(m, <double>(n-m-1), x) * eval_genlaguerre(m-1, <double>(n-m), x)
                    if l0 != 0.0:
                        l0 = 4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


cdef double wm_mn(double x, double mu):
    """
    """
    cdef double p0, p1, p2, l0, l1, l2, d_x_mu_n_m
    cdef signed long m, n
    cdef signed long nmax, mmax
    cdef double s = 0.0

    mmax = m_max(x, mu)
    for m in range(mmax+1):
        nmax = n_max(x, mu, m)
        for n in range(nmax+1):
            if (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p0 = pow_multifactorial(x, n-1, m-1)
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(n-2, <double>(m-n+1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(n, <double>(m-n), x)
                    l1 = p1 * l1 * l1

                    l0 = eval_genlaguerre(n-1, <double>(m-n+1), x) * eval_genlaguerre(n-1, <double>(m-n), x)
                    if l0 != 0.0:
                        l0 = -4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
                else:
                    # n > m
                    p0 = pow_multifactorial(x, m, n-1)
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(m-1, <double>(n-m-1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(m, <double>(n-m), x)
                    l1 = p1 * l1 * l1

                    l0 = eval_genlaguerre(m, <double>(n-m-1), x) * eval_genlaguerre(m-1, <double>(n-m), x)
                    if l0 != 0.0:
                        l0 = 4.0 * p0 * x * l0

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += ( l0 + (x - mu - n + m + 0.5) * (l1 + l2) ) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


cdef double wz_nm(double x, double mu):
    """
    """
    cdef double p1, p2, l1, l2, d_x_mu_n_m
    cdef signed long m, n
    cdef signed long nmax, mmax
    cdef double s = 0.0

    nmax = n_max(x, mu)
    for n in range(nmax+1):
        mmax = m_max(x, mu, n)
        for m in range(mmax+1):
            if  (sqrt(x) - sqrt(<double>n + mu - 0.5) - sqrt(<double>m) >= 0.0):
                if m >= n:
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(n-2, <double>(m-n+1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(n, <double>(m-n), x)
                    l1 = p1 * l1 * l1

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += (x - mu - n + m + 0.5) * (l1 + l2) / d_x_mu_n_m
                else:
                    # n > m
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(m-1, <double>(n-m-1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(m, <double>(n-m), x)
                    l1 = p1 * l1 * l1

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += (x - mu - n + m + 0.5) * (l1 + l2) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


cdef double wz_mn(double x, double mu):
    """
    """
    cdef double p1, p2, l1, l2, d_x_mu_n_m
    cdef signed long m, n
    cdef signed long nmax, mmax
    cdef double s = 0.0

    mmax = m_max(x, mu)
    for m in range(mmax+1):
        nmax = n_max(x, mu, m)
        for n in range(nmax+1):
            if (sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m) >= 0.0):
                if m >= n:
                    p1 = pow_multifactorial(x, n, m)
                    p2 = pow_multifactorial(x, n-2, m-1)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(n-2, <double>(m-n+1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(n, <double>(m-n), x)
                    l1 = p1 * l1 * l1

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) * 
                    s += (x - mu - n + m + 0.5) * (l1 + l2) / d_x_mu_n_m
                else:
                    # n > m
                    p1 = pow_multifactorial(x, m, n)
                    p2 = pow_multifactorial(x, m-1, n-2)

                    if isfinite(p2) and (n > 1):
                        l2 = eval_genlaguerre(m-1, <double>(n-m-1), x)
                        l2 = p2 * l2 * l2
                    else:
                        l2 = 0.0

                    l1 = eval_genlaguerre(m, <double>(n-m), x)
                    l1 = p1 * l1 * l1

                    d_x_mu_n_m = d(x, mu, n, m)
                    # theta(sqrt(x) - sqrt(n + mu - 0.5) - sqrt(m)) *
                    s += (x - mu - n + m + 0.5) * (l1 + l2) / d_x_mu_n_m
    
    return Kfp*exp(-x)/sqrt(mu*x) * s


# cdef double bounds_gen(double x_max, double mu):
#     """
#     b = (sqrt{m} + \sqrt{n + \mu - 1/2})^2 = m + n + \mu - 1/2 + 2 \sqrt{m (n + \mu - 1/2)} @ x >= \mu - 1/2
#     """
#     cdef signed long m, n, nmax, mmax
#     cdef double x

#     nmax = n_max(x_max, mu)
#     for n in range(nmax+1):
#         mmax = m_max(x_max, mu, n)
#         for m in range(mmax+1):
#             x = <double>n + mu - 0.5
#             # if m != 0:
#             x = <double>m + x + 2.0 * sqrt(<double>m * x)
#             if x >= mu - 0.5:
#                 yield x
#             else:
#                 continue
#     yield x


def bounds_gen(double x_max, double mu):
    """
    b = (sqrt{m} + \sqrt{n + \mu - 1/2})^2 = m + n + \mu - 1/2 + 2 \sqrt{m (n + \mu - 1/2)} @ x >= \mu - 1/2
    """
    # cdef signed long m, n, nmax, mmax
    # cdef double x

    nmax = n_max(x_max, mu)
    print(x_max, mu, nmax)
    for n in range(nmax+1):
        mmax = m_max(x_max, mu, n)
        for m in range(mmax+1):
            x = n + mu - 0.5
            # if m != 0:
            x = m + x + 2.0 * sqrt(m * x)
            if x >= mu - 0.5:
                yield x
            else:
                continue
    yield x


def gen_wp_nm(mu):
    """
    returns closure for wp_nm(x, mu=mu)
    """
    # from calcfunctions import wp_nm
    wp_nm.__defaults__ = (mu, )
    return wp_nm


def gen_wp_mn(mu):
    """
    returns closure for wp_mn(x, mu=mu)
    """
    # from calcfunctions import wp_nm
    wp_mn.__defaults__ = (mu, )
    return wp_mn


def gen_wm_nm(mu):
    """
    returns closure for wm_nm(x, mu=mu)
    """
    # from calcfunctions import wm_nm
    wm_nm.__defaults__ = (mu, )
    return wm_nm


def gen_wm_mn(mu):
    """
    returns closure for wm_mn(x, mu=mu)
    """
    # from calcfunctions import wm_nm
    wm_mn.__defaults__ = (mu, )
    return wm_mn


def gen_wz_nm(mu):
    """
    returns closure for wz_nm(x, mu=mu)
    """
    # from calcfunctions import wp_nm
    wz_nm.__defaults__ = (mu, )
    return wz_nm


def gen_wz_mn(mu):
    """
    returns closure for wz_mn(x, mu=mu)
    """
    # from calcfunctions import wp_nm
    wz_mn.__defaults__ = (mu, )
    return wz_mn

cdef double _phi_eta

# cdef double phi(double y, double eta):
cdef double phi(double y):
    """
    auxiliary function to integrate for wPhi_lin(x, mu) and wPhi_log(x, mu)
    """
    global _phi_eta
    cdef double eta
    eta = _phi_eta

    cdef double thy, y_m_thy, y_thy, x, z

    if y == 0.0:
        return 0.0

    thy = tanh(y)
    y_m_thy = y - thy
    y_thy = y * thy

    x = exp(-2.0 * y)
    z = x + (1.0 + x) * y - 1.0 # (1+x) * y - (1-x)

    return (1.0 - x*x - 4.0*x*y) / (z * sqrt(y * z * (1.0-x))) * exp(-y_thy / (eta * y_m_thy))


cdef double wPhi_lin(double x, double mu):
    """
    """
    # cdef double eta
    # eta = 2.0 * x / (mu*mu)

    global _phi_eta
    _phi_eta = 2.0 * x / (mu*mu)

    return KPhi * sqrt(mu) / x * integrate(phi, 0.0E+00, 1.0E+05, n=320000)


cdef double wPhi_log(double x, double mu):
    """
    """
    # cdef double eta
    # eta = 2.0 * x / (mu*mu)

    global _phi_eta
    _phi_eta = 2.0 * x / (mu*mu)

    return KPhi * sqrt(mu) / x * logintegrate(phi, 1.0E-05, 1.0E+05, n=64000)


cdef double _Fv_chi, _Fv_lambda
_Fv_lambda = _lambda

# cdef double Fv(double v, double chi, double _lambda):
cdef double Fv(double v):
    """
    auxiliary function to integrate for wFv_lin(x, mu) and wFv_log(x, mu) [1]
    """
    global _Fv_chi, _Fv_lambda
    cdef double chi, _lambda
    chi = _Fv_chi
    _lambda = _Fv_lambda

    cdef double one_minus_v, v_p_lambda_1mv, u

    one_minus_v = 1.0 - v
    v_p_lambda_1mv = v + _lambda * one_minus_v
    u = 2.0 * v_p_lambda_1mv * sqrt(v_p_lambda_1mv) / (3.0 * chi * v * one_minus_v)

    return (
        v_p_lambda_1mv * (2.0 * (1.0 + v) * (2.0 + v) + _lambda * one_minus_v * (2.0 - v))
    ) * kv(2.0/3.0, u) / (v * one_minus_v * one_minus_v)


cdef double wFv_lin(double x, double mu):
    """
    """
    # cdef double chi
    # chi = sqrt(x) / (2.0 * mu * sqrt(mu))

    global _Fv_chi
    _Fv_chi = sqrt(x) / (2.0 * mu * sqrt(mu))
    return KFv * sqrt(mu/x) * integrate(Fv, 1.0E-20, 1.0-1.0E-12, n=320000)


cdef double wFv_log(double x, double mu):
    """
    """
    # cdef double chi
    # chi = sqrt(x) / (2.0 * mu * sqrt(mu))

    global _Fv_chi
    _Fv_chi = sqrt(x) / (2.0 * mu * sqrt(mu))
    return KFv * sqrt(mu/x) * logintegrate(Fv, 1.0E-20, 1.0-1.0E-12, n=65536)

cdef double _Ftau_chi, _Ftau_b
_Ftau_b = b

# cdef double Ftau(double tau, double chi, double b):
cdef double Ftau(double tau):
    """
    auxiliary function to integrate for wFtau_lin(x, mu) and wFtau_log(x, mu) [2]
    """
    global _Ftau_chi, _Ftau_b
    cdef double chi, b

    if tau == 0.0:
        return 0.0

    chi = _Ftau_chi
    b = _Ftau_b
    print(chi, b)

    cdef double tau_plus_b, tau_plus_1, u

    tau_plus_b = tau + b
    tau_plus_1 = tau + 1.0
    u = 2.0 * sqrt(tau_plus_b*tau_plus_1) * tau_plus_1 / (3.0 * chi * tau * b)

    return (
        tau_plus_1 * (2.0 * (2.0*tau + b) * (3.0*tau + 2.0*b) + (tau + 2.0*b))
    ) * kv(2.0/3.0, u) / (tau * tau_plus_b * tau_plus_b)


cdef double wFtau_lin(double x, double mu):
    """
    """
    # cdef double chi
    # chi = sqrt(x) / (2.0 * mu * sqrt(mu))

    global _Ftau_chi
    _Ftau_chi = sqrt(x) / (2.0 * mu * sqrt(mu))
    return KFtau * sqrt(mu/x) * integrate(Ftau, 1.0E-16, 1.0E+03*b, n=320000)


cdef double wFtau_log(double x, double mu):
    """
    """
    # cdef double chi
    # chi = sqrt(x) / (2.0 * mu * sqrt(mu))

    global _Ftau_chi
    _Ftau_chi = sqrt(x) / (2.0 * mu * sqrt(mu))
    return KFtau * sqrt(mu/x) * logintegrate(Ftau, 1.0E-16, 1.0E+03*b, n=65536)

# ctypedef double (*func_d_d)(double, double)
ctypedef double (*func_d_d)(double)

# cdef double integrate(func_d_d f, double func_second_param, double minvalue, double maxvalue, size_t n=32000):
cdef double integrate(func_d_d f, double minvalue, double maxvalue, size_t n=32000):
    '''
    Integration f over x in linear scale.
    Values are summarized in n points.
    '''
    cdef double delta, dx, x_curr, x_prev, S
    cdef size_t i

    delta = maxvalue - minvalue
    dx = delta / <double>n

    S = 0.0

    # Trapeze
    x_curr = minvalue
    for i in range(n):
        x_prev = x_curr
        x_curr = minvalue + (<double>i+1.0) * dx
        x = minvalue + (<double>i+0.5) * dx
        # S += (0.25 * (f(x_prev, func_second_param) + f(x_curr, func_second_param)) + 0.5*f(x, func_second_param)) * dx
        S += (0.25 * (f(x_prev) + f(x_curr)) + 0.5*f(x)) * dx

    return S


# cdef double logintegrate(func_d_d f, double func_second_param, double minvalue, double maxvalue, size_t n=32000):
cdef double logintegrate(func_d_d f, double minvalue, double maxvalue, size_t n=32000):
    '''
    Integration f over x in logarithmic scale.
    Values are summarized in n points.
    '''
    cdef double xi_min, xi_max, delta_xi, d_xi, xi_curr, xi_prev, S
    cdef size_t i

    xi_min = log(minvalue)
    xi_max = log(maxvalue)

    delta_xi = xi_max - xi_min
    d_xi = delta_xi / <double>n

    S = 0.0

    # Trapeze
    xi_curr = xi_min

    for i in range(n):
        xi_prev = xi_curr
        xi_curr = xi_min + (<double>i+1.0) * d_xi
        xi = xi_min + (<double>i+0.5) * d_xi

        # S += (
        #     0.25 * (
        #         f(exp(xi_prev), func_second_param) * exp(xi_prev) +
        #         f(exp(xi_curr), func_second_param) * exp(xi_curr)
        #     )
        #     + 0.5 * f(exp(xi), func_second_param) * exp(xi)
        # ) * d_xi

        S += (
            0.25 * (
                f(exp(xi_prev)) * exp(xi_prev) +
                f(exp(xi_curr)) * exp(xi_curr)
            )
            + 0.5 * f(exp(xi)) * exp(xi)
        ) * d_xi

    return S
