#!/usr/bin/env python3

def inc(x):
	return(x+1)

def origfunc(a, b=None):
	print("{}: `{}`".format(a, b))


# curring
def gen_a(b):
	def retf(a):
		return origfunc(a, b)
	return retf

def gen_a_lambda(b):
	return lambda a: origfunc(a, b)


# closure
def gen_b(b):
	def origfunc(a, b=b):
		print("{}: `{}`".format(a, b))
	return origfunc

def gen_b_lambda(b):
	return lambda a: print("{}: `{}`".format(a, b))


# partial
from functools import partial
f_partial = partial(origfunc, b="q")

# or functor
class origFunctor:
	def __init__(self, b):
		self.b = b
	def __call__(self, a):
		print("{}: `{}`".format(a, self.b))

f_functor = origFunctor("q")


# again closure
b = "q"
def origf(a, b=b):
	print("{}: `{}`".format(a, b))

def gen_c(b):
	from __main__ import origf
	return origf

# ===============================

def of(x, y):
	return x * y

# closures:
def g_closure_1(y):
	def of(x, y=y):
		return x * y
	return of

def g_closure_2(y):
	return lambda x: x * y

_n = 1.0
def f_closure_3(x, y=_n):
	return x * y

def g_closure_4(y):
	_n = y
	# from __main__ import f_closure_3
	return f_closure_3

def g_closure_5(y):
	# from __main__ import of
	# import copy
	# of_copy = copy.deepcopy(of)
	of_copy = of
	of_copy.__defaults__ = (y, )
	return of_copy

# partials:
f_partial_1 = partial(of, y=1.0)

# functors:
class oFunctor:
	def __init__(self, y):
		self.y = y
	def __call__(self, x):
		return x * self.y

f_functor_1 = oFunctor(1.0)

# curring:
def g_curring_1(y):
	def retf(x):
		return of(x, y)
	return retf

def g_curring_2(y):
	return lambda x: of(x, y)

def test(x, z):
	def inner_test_func(y):
		# print(x, y)
		x = 7
		print(x, y)

		return
	print(x)
	inner_test_func(5)
	print(x)
	return z


def timing():
	from time import time
	last = None
	while True:
		curr = time()
		(curr, last, ) = (curr-last, curr, ) if last else (0.0, curr, )
		yield curr


# ======================================================================
# 1
def fact_a(n):
	return n * fact(n-1) if n > 0 else 1

# 2
fact_b = lambda self, n: n * self(self, n-1) if n > 0 else 1

# 3
wrap = lambda fn, *args: fn(fn, *args)
fact_c = lambda self, n: n * self(self, n-1) if n > 0 else 1
# fact = lambda n: fact_c(fact_c, n)
# print(wrap(fact_c, 5))

# 4
# Y-combinator
Y = lambda fn: lambda *args: fn(fn, *args)
fact_d = Y(lambda self, n: n * self(self, n-1) if n > 0 else 1)

fact = fact_d
# print(fact(6))
# ======================================================================


if __name__ == '__main__':

	import wnuetowpluseminus
	print("started!")
	g = wnuetowpluseminus.plots_log[0]

	wnuetowpluseminus.plt_energy_lambda(
        s=g['s'],
        x_max=g['x_max'],
        points_in_interval=2000,
        logscale=g['logscale'],
        y_lim=g['y_lim'],
        yl_lim=g['yl_lim'],
        # filename_postfix='',
        filename_postfix='w-integr-1',
    )

	# from time import sleep
	# timing = timing()
	# print(timing.__next__())
	# print(timing.__next__())
	# print(next(timing))
	# print(next(timing))
	# sleep(1)
	# print(next(timing))
	# sleep(2)
	# print(next(timing))
	# test(2, 3)
	exit()
	f = gen_a("q")
	f("Vot")
	f = gen_a_lambda("q")
	f("Vot")
	f = gen_b("q")
	f("Vot")
	f = gen_b_lambda("q")
	f("Vot")
	f_partial("Vot")
	f_functor("Vot")
	origf("Vot")
	f = gen_c("q")
	f("Vot")
	# exit()

	import timeit

	# 
	print("i = i+1:")
	ti = timeit.repeat('i=i+1', setup='i=0', repeat=3, number=10000000)
	print(ti)
	print("i += 1:")
	ti = timeit.repeat('i+=1', setup='i=0', repeat=3, number=10000000)
	print(ti)
	print("i += 1.0:")
	ti = timeit.repeat('i+=1', setup='i=0.0', repeat=3, number=10000000)
	print(ti)
	print("i = j:")
	ti = timeit.repeat('i=j', setup='i=0;j=1', repeat=3, number=10000000)
	print(ti)
	print("i, j = j, i:")
	ti = timeit.repeat('i,j=j,i', setup='i=0;j=1', repeat=3, number=10000000)
	print(ti)
	print("i *= 1.0:")
	ti = timeit.repeat('i*=1.0', setup='i=1.0', repeat=3, number=10000000)
	print(ti)
	print("i *= 1:")
	ti = timeit.repeat('i*=1', setup='i=1.0', repeat=3, number=10000000)
	print(ti)
	print("i *= 2.0:")
	ti = timeit.repeat('i*=2.0', setup='i=1.0', repeat=3, number=10000000)
	print(ti)
	print("i *= 2:")
	ti = timeit.repeat('i*=2', setup='i=1.0', repeat=3, number=10000000)
	print(ti)
	# 
	print("inc(i) [lambda]:")
	ti = timeit.repeat('inc(i)', setup='i=0;inc=lambda x: x+1', repeat=3, number=10000000)
	print(ti)
	print("inc(i) [def\import from __main__]:")
	ti = timeit.repeat('inc(i)', setup='i=0;from __main__ import inc', repeat=3, number=10000000)
	print(ti)
	# 
	print("i=inc(i) [lambda]:")
	ti = timeit.repeat('i=inc(i)', setup='i=0;inc=lambda x: x+1', repeat=3, number=10000000)
	print(ti)
	print("inc(i) [def\import from __main__]:")
	ti = timeit.repeat('i=inc(i)', setup='i=0;from __main__ import inc', repeat=3, number=10000000)
	print(ti)
	print("z=of(1.0, 1.0):")
	ti = timeit.repeat('z=of(1.0, 1.0)', setup='from __main__ import of', repeat=3, number=10000000)
	print(ti)
	print("z=f_closure_1(1.0):")
	ti = timeit.repeat('z=f_closure_1(1.0)', setup='from __main__ import g_closure_1;f_closure_1 = g_closure_1(1.0)', repeat=3, number=10000000)
	print(ti)
	print("z=f_closure_2(1.0):")
	ti = timeit.repeat('z=f_closure_2(1.0)', setup='from __main__ import g_closure_2;f_closure_2 = g_closure_2(1.0)', repeat=3, number=10000000)
	print(ti)
	print("z=f_closure_3(1.0):")
	ti = timeit.repeat('z=f_closure_3(1.0)', setup='from __main__ import f_closure_3;_n=1.0', repeat=3, number=10000000)
	print(ti)
	print("z=f_closure_4(1.0):")
	ti = timeit.repeat('z=f_closure_4(1.0)', setup='from __main__ import g_closure_4;f_closure_4 = g_closure_4(1.0)', repeat=3, number=10000000)
	print(ti)
	print("z=f_closure_5(1.0):")
	ti = timeit.repeat('z=f_closure_5(1.0)', setup='from __main__ import g_closure_5;f_closure_5 = g_closure_5(1.0)', repeat=3, number=10000000)
	print(ti)
	print("z=f_partial_1(1.0):")
	ti = timeit.repeat('z=f_partial_1(1.0)', setup='from __main__ import f_partial_1', repeat=3, number=10000000)
	print(ti)
	print("z=f_functor_1(1.0):")
	ti = timeit.repeat('z=f_functor_1(1.0)', setup='from __main__ import f_functor_1', repeat=3, number=10000000)
	print(ti)
	print("z=f_curring_1(1.0):")
	ti = timeit.repeat('z=f_curring_1(1.0)', setup='from __main__ import g_curring_1;f_curring_1 = g_curring_1(1.0)', repeat=3, number=10000000)
	print(ti)
	print("z=f_curring_2(1.0):")
	ti = timeit.repeat('z=f_curring_2(1.0)', setup='from __main__ import g_curring_2;f_curring_2 = g_curring_2(1.0)', repeat=3, number=10000000)
	print(ti)
