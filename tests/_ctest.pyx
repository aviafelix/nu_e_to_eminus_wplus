#!/usr/bin/env cython
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: cdivision=True
import cython
from  _calcfunctions cimport *
from _calcfunctions import *

def __main__():
	print(wPhi_log(77.0, mu(0.01)))
	print(wFv_log(77.0, mu(0.01)))
	print(wFtau_log(77.0, mu(0.01)))
	print("===")
	print(mu(0.1))
	print(mu(0.01))
	print("===")
	print(wp_nm(77.0, mu(0.01)))
	print(wm_nm(77.0, mu(0.01)))
	print(wz_nm(77.0, mu(0.01)))
	print("===")
	print(wp_nm(77.0, mu(0.1)))
	print(wm_nm(77.0, mu(0.1)))
	print(wz_nm(77.0, mu(0.1)))

if __name__ == '__main__':
	__main__()
