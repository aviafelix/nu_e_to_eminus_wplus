Calculations for the ![$\nu_e \to W^+ \, e^-$](//tex.s2cms.ru/svg/%24%5Cnu_e%20%5Cto%20e%5E-%20%5C%2C%20W%5E%2B%24) Process in the Strong Magnetic Field
================================================================================

Python tool for making plots of the probability (decay width) and mean free paths for different values of the magnetic field.

![qpam.ru link](//id.qpam.ru/id/bitbucket.org/aviafelix/nu_e_to_eminus_wplus.png)