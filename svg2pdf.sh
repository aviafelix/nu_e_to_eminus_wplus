#!/bin/sh
for filename in *.svg; do rsvg-convert -f pdf $filename > ${filename%.svg}.rsvg-1.pdf; done
for filename in *.svg; do rsvg-convert -f eps $filename > ${filename%.svg}.rsvg.eps; done