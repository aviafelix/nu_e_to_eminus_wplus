#!/usr/bin/env python3
files = [
    'decay_width-s_0.90000-epsmax_5.0-p_2000-w-integr-eps-wm-final-linear.svg',
    'decay_width-s_0.90000-epsmax_5.0-p_2000-w-integr-eps-wm-final-log.svg',
    'decay_width-s_0.90000-epsmax_5.0-p_2000-w-integr-x-wm-final-linear.svg',
    'decay_width-s_0.90000-epsmax_5.0-p_2000-w-integr-x-wm-final-log.svg',
    'lambda-s_0.90000-epsmax_5.0-p_2000-w-integr-eps-wm-final-linear.svg',
    'lambda-s_0.90000-epsmax_5.0-p_2000-w-integr-eps-wm-final-log.svg',
    'lambda-s_0.90000-epsmax_5.0-p_2000-w-integr-x-wm-final-linear.svg',
    'lambda-s_0.90000-epsmax_5.0-p_2000-w-integr-x-wm-final-log.svg',
]

import cairosvg

for file in files:
  with open(file, 'rb') as f:
    cairosvg.svg2pdf(file_obj=f, write_to=file+'.pdf')
