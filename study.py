#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import plotsettings
import json5 as json
from math import *

DATAPATH = '_bin/s_0.05000-epsmax_5.0-p_200-w-integr-wm-final/'
OUTPUT_FILE = '_images/output.png'
s = 1.0/20.0
mu = 10.0

x_min = 0.0
x_max = 40.0

y_min = 0.0
y_max = 2.76E-03
# y_min = 1.0E-06
# y_max = 2.0E-01

plot_settings_list = ['k-', ]
plot_settings_dict = {}

def n_max(x, mu, m=0):
    """
    n_max = (\sqrt{x} - \sqrt{m})^2 - \mu + 1/2
    """
    if m == 0:
        return floor(x-mu+0.5)
    else:
        t = sqrt(x) - sqrt(m)
        return floor(t*t - mu + 0.5)

def m_max(x, mu, n=0):
    """
    m_max = (\sqrt{x} - \sqrt{n + \mu - 1/2})^2
    """
    t = sqrt(x) - sqrt(n+mu-0.5)
    return floor(t*t)

def __main__():
    x = np.fromfile(DATAPATH+'x_s.bin')
    y = np.fromfile(DATAPATH+'decay_width_s.bin')
    bounds = np.fromfile(DATAPATH+'bounds.bin')

    cond = np.argsort(x)
    x = x[cond]
    y = y[cond]


    # x = np.append(x, np.full(len(bounds), np.nan))
    x = np.append(x, bounds)
    y = np.append(y, np.full(len(bounds), np.nan))
    cond = np.argsort(x)
    x = x[cond]
    y = y[cond]

    cond = np.logical_or(
        np.isnan(x),
        # np.logical_and(x >= x_min, x <= x_max),
        np.logical_and(x >= -np.inf, x <= np.inf),
    )

    x = x[cond]
    y = y[cond]

    cond = np.logical_or(
        np.isnan(y),
        # np.logical_and(y >= y_min, y <= y_max),
        np.logical_and(y >= -np.inf, y <= np.inf),
    )

    x = x[cond]
    y = y[cond]

    fig, ax = plt.subplots()
    # ax.set_title('')
    # ax.set_xlabel('')
    # ax.set_ylabel('')
    ax.set_xlim([x_min, x_max])
    ax.set_ylim([y_min, y_max])

    # ax.set_xscale('log')
    # ax.set_yscale('log')

    n_max_max = n_max(x_max, mu)
    m_max_max = m_max(x_max, mu)

    ## ===================================
    for n in range(0, n_max(x_max, mu)+2):
        for m in range(0, m_max(x_max, mu, n=n)+1):
            t = sqrt(1.0*m)+sqrt(1.0*n+mu-0.5)
            ax.axvline(x=t*t, linestyle='--', color=(0.7, 0.7, 0.7))

    # for m in range(0, m_max(x_max, mu)+2):
    #     for n in range(0, n_max(x_max, mu, m=m)+1):
    #         t = sqrt(1.0*m)+sqrt(1.0*n+mu-0.5)
    #         ax.axvline(x=t*t, linestyle='--', color=(0.7, 0.7, 0.7))

    ## ===================================
    # for n in range(0, n_max(x_max, mu)+2):
    #     for m in range(1, m_max(x_max, mu, n=n)+1):
    #         t = sqrt(1.0*m)-sqrt(1.0*n+mu-0.5)
    #         ax.axvline(x=t*t, linestyle='--', color=(0.7, 0.7, 0.7))

    # for m in range(1, m_max(x_max, mu)+2):
    #     for n in range(0, n_max(x_max, mu, m=m)+1):
    #         t = sqrt(1.0*m)-sqrt(1.0*n+mu-0.5)
    #         ax.axvline(x=t*t, linestyle='--', color=(0.7, 0.7, 0.7))

    ## ===================================
    for j in range(50):
        ax.axvline(x=1.0*j+mu-0.5, linestyle='--', color=(0.4, 0.7, 1.0))

    for j in range(50):
        t = sqrt(1.0*j)+sqrt(mu-0.5)
        ax.axvline(x=t*t, linestyle='--', color=(1.0, 0.7, 0.4))

    ## [wrong]
    # for j in range(100):
    #     t = sqrt(1.0*j)-sqrt(mu-0.5)
    #     ax.axvline(x=t*t, linestyle='--', color=(0.7, 1.0, 0.4))

    ## plot n_max and m_max
    # ax.plot(x, np.vectorize(n_max)(x, mu)*(y_max/n_max_max), color=(0.2, 0.5, 0.9))
    # ax.plot(x, np.vectorize(m_max)(x, mu)*(y_max/m_max_max), color=(0.9, 0.5, 0.2))

    # ax.plot(x, y, *plot_settings_list, **plot_settings_dict)
    ax.plot(x, y, 'k-')


    fig.savefig(OUTPUT_FILE)
    plt.close(fig)

if __name__ == '__main__':
    __main__()
