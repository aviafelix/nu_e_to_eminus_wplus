#!/bin/sh
(./w_nue-to-Wplus_eminus.py --field-strength=0.1 --x-max=30 --points-in-interval=2000 --logscale > _1.log 2>&1 & ) &
(./w_nue-to-Wplus_eminus.py --field-strength=0.05 --x-max=45 --points-in-interval=2000 --logscale > _2.log 2>&1 & ) &
(./w_nue-to-Wplus_eminus.py --field-strength=0.03 --x-max=50 --points-in-interval=2000 --logscale > _3.log 2>&1 & ) &
(./w_nue-to-Wplus_eminus.py --field-strength=0.02 --x-max=70 --points-in-interval=2000 --logscale > _4.log 2>&1 & ) &
(./w_nue-to-Wplus_eminus.py --field-strength=0.0125 --x-max=90 --points-in-interval=2000 --logscale > _5.log 2>&1 & ) &
(./w_nue-to-Wplus_eminus.py --field-strength=0.01 --x-max=100 --points-in-interval=2000 --logscale > _6.log 2>&1 & ) &

# (./w_nue-to-Wplus_eminus.py  >> _0.log 2>&1 & ) &
