#!/bin/sh
echo ${1%.eps}
gs \
 -dBATCH -dNOPAUSE -dSAFER -dQUIET \
 -sDEVICE=pdfwrite \
 -sOutputFile=${1%.eps}.pdf \
 -dCompatibilityLevel=1.4 \
 -dPDFUseOldCMS=false \
 -dUseCropBox=false \
 -dEPSCrop \
  $1
