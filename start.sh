#!/bin/sh
# srun -n 1 -pdebug python3 w_nue-to-Wplus_eminus.py >> _001-debug.log 2>&1 &
# srun -n 1 --cpus-per-task=1 --threads-per-core=1 -pmain python3 w_nue-to-Wplus_eminus.py >> _001-logspace-wm-main.log 2>&1 &
# sbatch \
srun \
	--ntasks-per-core=1 \
	--ntasks-per-node=1 \
	--cpus-per-task=1 \
	--threads-per-core=1 \
	-pmain \
	calculate.pbs
	# -pdebug \
	# python3 plotter.py plots-final-log-scale.json5 >> _log1_log.log 2>&1 &
	# python3 plotter.py plots-final-linear-scale.json5 >> _log1_lin.log 2>&1 &
	# python3 w_nue-to-Wplus_eminus.py >> _002-logspace-wm-main.log 2>&1 &
