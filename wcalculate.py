#!/usr/bin/env python3
import plotsettings
from calcfunctions import *
import numpy as np
# import pandas as pd
import matplotlib.pyplot as plt
# from itertools import islice
# from cycler import cycler
import plotsettings
import argparse

# %matplotlib inline
# %config InlineBackend.figure_format = 'svg'
# # %load_ext autoreload
# # %autoreload 0

def timing():
    from time import time
    last = None
    while True:
        curr = time()
        (curr, last, ) = (curr-last, curr, ) if last else (0.0, curr, )
        yield curr


def parse_args():
    parser = argparse.ArgumentParser(description="electron neutrino energy and mean free path calculations")
    parser.add_argument(
        '-s',
        '--field-strength',
        type=float,
        help="dimensionless field strength in m_W^2 units",
    )
    parser.add_argument(
        '-x',
        '--x-max',
        type=float,
        help="maximal (dimensionless, in m_W^2) value of the squared neutrino momentum at which values are calculated",
    )
    parser.add_argument(
        '-p',
        '--points-in-interval',
        type=int,
        default=50,
        help="number of points to calculate in every interval",
    )
    parser.add_argument(
        '--logscale',
        # default=True,
        action='store_true',
        help="draw plots in logarithmic scale",
    )
    return parser.parse_args()


def plt_energy_lambda(
        s,
        x_max,
        points_in_interval=50,
        logscale=True,
        y_lim=None,
        yl_lim=None,
        filename_postfix='',
        w_func=wp_nm,
    ):
    """
    """
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()

    _timing = timing()
    next(_timing)

    remark = 'logarithmic scale' if logscale else 'linear scale'

    ax1.set_title('Decay width $w(x)$ for $\\beta = {:.03g} \\, m_W^2$; {}'.format(s, remark))
    ax1.set_xlabel('Transversal momentum, $x = p_\\perp^2/(2 \\beta)$')
    ax1.set_ylabel('Decay width $w(x)$, [GeV]')

    ax2.set_title('Mean free path $\\lambda$ for $\\beta = {:.03g} \\, m_W^2$; {}'.format(s, remark))
    ax2.set_xlabel('Transversal momentum, $x = p_\\perp^2/(2 \\beta)$')
    ax2.set_ylabel('$\\lambda$, [cm]')

    x_min = mu(s) - 0.5

    bounds = list(set([x for x in bounds_gen(x_max, mu(s))]))
    bounds.sort()
    bounds_iter = iter(bounds)

    np.array(bounds).tofile(
        'bounds-s_{:.05f}-xmax_{}-p_{}{}.bin'.format(
            s, x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        )
    )

    x_left = next(bounds_iter)

    y_plt_min = float('inf')
    y_plt_max = float('-inf')
    yl_plt_min = float('inf')
    yl_plt_max = float('-inf')

    filenames = {
        'decay_width': 'decay_width_s-s_{:.05f}-xmax_{}-p_{}{}.bin'.format(
            s, x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
        'lambda': 'lambda_s-s_{:.05f}-xmax_{}-p_{}{}.bin'.format(
            s, x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
        'x': 'x_s-s_{:.05f}-xmax_{}-p_{}{}.bin'.format(
            s, x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
    }

    with open(filenames['decay_width'], 'wb') as f_dw, \
         open(filenames['lambda'], 'wb') as f_lambda, \
         open(filenames['x'], 'wb') as f_x:

        for xc in bounds_iter:
            eps = 1.0E-09 * (xc - x_left) / points_in_interval

            x = np.linspace(x_left+eps, xc-eps, points_in_interval, endpoint=True)
            y = np.vectorize(w_func)(x, mu(s))
            x.tofile(f_x)
            y.tofile(f_dw)

            y_plt_min = min(y.min(), y_plt_min)
            y_plt_max = max(y.max(), y_plt_max)

            ax1.plot(x, y, 'b-')

            # lambda:
            y = K_lambda/y
            y.tofile(f_lambda)

            yl_plt_min = min(y.min(), yl_plt_min)
            yl_plt_max = max(y.max(), yl_plt_max)

            ax2.plot(x, y, 'b-')

            x_left = xc

    print("decay width (sum) was plotted in {} seconds".format(next(_timing)))

    # ==============================================================
    # plot line by integration the expression from paper [KuznMikhSergh] (1)
    filenames = {
        'decay_width': 'decay_width_i-s_{:.05f}-xmax_{}-p_{}{}.bin'.format(
            s, x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
        'lambda': 'lambda_i-s_{:.05f}-xmax_{}-p_{}{}.bin'.format(
            s, x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
        'x': 'x_i-s_{:.05f}-xmax_{}-p_{}{}.bin'.format(
            s, x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
    }

    with open(filenames['decay_width'], 'wb') as f_dw, \
         open(filenames['lambda'], 'wb') as f_lambda, \
         open(filenames['x'], 'wb') as f_x:

        x = np.linspace(x_min, x_max, points_in_interval, endpoint=True)
        y = np.vectorize(wPhi_log)(x, mu(s))
        x.tofile(f_x)
        y.tofile(f_dw)

        y_plt_min = min(y.min(), y_plt_min)
        y_plt_max = max(y.max(), y_plt_max)
        ax1.plot(x, y, 'r-')

        # lambda:
        y = K_lambda/y
        y.tofile(f_lambda)

        yl_plt_min = min(y.min(), yl_plt_min)
        yl_plt_max = max(y.max(), yl_plt_max)
        ax2.plot(x, y, 'r-')

        # begin from zero
        x = np.linspace(1.0E-06, x_min, points_in_interval, endpoint=True)
        y = np.vectorize(wPhi_log)(x, mu(s))
        x.tofile(f_x)
        y.tofile(f_dw)

        ax1.plot(x, y, 'r--')

        # lambda:
        y = K_lambda/y
        y.tofile(f_lambda)

        ax2.plot(x, y, 'r--')

    print("decay width (integral 1) was plotted in {} seconds".format(next(_timing)))
    x_min = 0.0
    # ==============================================================

    # # ==============================================================
    # # plot line by integration the expression from paper [KuznMikhSergh] (2)
    # x = np.linspace(x_min, x_max, points_in_interval, endpoint=True)
    # # y = np.vectorize(wFtau_log)(x, mu(s))
    # y = np.vectorize(wFv_log)(x, mu(s))
    # y_plt_min = min(y.min(), y_plt_min)
    # y_plt_max = max(y.max(), y_plt_max)
    # ax1.plot(x, y, '-.', color='#aaaa33')

    # # lambda:
    # y = K_lambda/y
    # yl_plt_min = min(y.min(), yl_plt_min)
    # yl_plt_max = max(y.max(), yl_plt_max)
    # ax2.plot(x, y, '-.', color='#aaaa33')

    # # begin from zero
    # x = np.linspace(1.0E-12, x_min, points_in_interval, endpoint=True)
    # # y = np.vectorize(wFtau_log)(x, mu(s))
    # y = np.vectorize(wFv_log)(x, mu(s))
    # ax1.plot(x, y, '-.', color='#aaaa33')

    # # lambda:
    # y = K_lambda/y
    # ax2.plot(x, y, '-.', color='#aaaa33')

    # print("decay width (integral 2) was plotted in {} seconds".format(next(_timing)))
    # x_min = 0.0
    # # ==============================================================

    if logscale:
        # ax1.set_xscale('log')
        ax1.set_yscale('log')
        # ax2.set_xscale('log')
        ax2.set_yscale('log')

    if y_lim is None:
        y_plt_min = math.floor(y_plt_min * math.pow(10, -math.floor(math.log10(y_plt_min)))) * math.pow(10, math.floor(math.log10(y_plt_min)))
        y_plt_max = math.ceil(y_plt_max * math.pow(10, -math.floor(math.log10(y_plt_max)))) * math.pow(10, math.floor(math.log10(y_plt_max)))

    if yl_lim is None:
        yl_plt_min = math.floor(yl_plt_min * math.pow(10, -math.floor(math.log10(yl_plt_min)))) * math.pow(10, math.floor(math.log10(yl_plt_min)))
        yl_plt_max = math.ceil(yl_plt_max * math.pow(10, -math.floor(math.log10(yl_plt_max)))) * math.pow(10, math.floor(math.log10(yl_plt_max)))

    ax1.set_xlim([x_min, x_max])
    ax2.set_xlim([x_min, x_max])

    ax1.set_ylim(y_lim) if y_lim else ax1.set_ylim([y_plt_min, y_plt_max])
    ax2.set_ylim(yl_lim) if yl_lim else ax2.set_ylim([yl_plt_min, yl_plt_max])

    ax1.grid()
    ax2.grid()

    # plt.legend()
    fig1.savefig("decay_width-{}-s_{:.05f}-xmax_{}-p_{}{}.png"
        .format(
            ('linscale', 'logscale')[logscale],
            s,
            x_max,
            points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        )
    )
    fig2.savefig("lambda-{}-s_{:.05f}-xmax_{}-p_{}{}.png"
        .format(
            ('linscale', 'logscale')[logscale],
            s,
            x_max,
            points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        )
    )
    print("image saved in {} seconds".format(next(_timing)))


def plt_eps_energy_lambda(
        s,
        x_max,
        points_in_interval=50,
        logscale=True,
        y_lim=None,
        yl_lim=None,
        filename_postfix='',
        w_func=wp_nm,
    ):
    """
    """
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()

    _timing = timing()
    next(_timing)

    remark = 'logarithmic scale' if logscale else 'linear scale'

    ax1.set_title('Decay width $w(x)$ for $\\beta = {:.03g} \\, m_W^2$; {}'.format(s, remark))
    ax1.set_xlabel('Energy, $\\varepsilon = E/m_W$')
    ax1.set_ylabel('Decay width $w(\\varepsilon)$, [GeV]')

    ax2.set_title('Mean free path $\\lambda$ for $\\beta = {:.03g} \\, m_W^2$; {}'.format(s, remark))
    ax2.set_xlabel('Energy, $\\varepsilon = E/m_W$')
    ax2.set_ylabel('$\\lambda$, [cm]')

    print("field strength s = {} m_W^2; epsilon_max = {}".format(s, x_max))

    _x_max = x_max # epsilon
    x_max = mu(s) * x_max * x_max
    x_min = mu(s) - 0.5

    print("mu(s): {}; x_max: {}".format(mu(s), x_max))

    bounds = list(set([x for x in bounds_gen(x_max, mu(s))]))
    bounds.sort()
    bounds_iter = iter(bounds)

    np.array(bounds).tofile(
        'bounds-s_{:.05f}-epsmax_{}-p_{}{}.bin'.format(
            s, _x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        )
    )

    print("function name: {}".format(w_func.__name__))
    print("bounds length: {}".format(len(bounds)))

    x_left = next(bounds_iter)

    y_plt_min = float('inf')
    y_plt_max = float('-inf')
    yl_plt_min = float('inf')
    yl_plt_max = float('-inf')

    filenames = {
        'decay_width': 'decay_width_s-s_{:.05f}-epsmax_{}-p_{}{}.bin'.format(
            s, _x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
        'lambda': 'lambda_s-s_{:.05f}-epsmax_{}-p_{}{}.bin'.format(
            s, _x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
        'x': 'x_s-s_{:.05f}-epsmax_{}-p_{}{}.bin'.format(
            s, _x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
        'epsilon': 'epsilon_s-s_{:.05f}-epsmax_{}-p_{}{}.bin'.format(
            s, _x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
    }

    with open(filenames['decay_width'], 'wb') as f_dw, \
         open(filenames['lambda'], 'wb') as f_lambda, \
         open(filenames['x'], 'wb') as f_x, \
         open(filenames['epsilon'], 'wb') as f_epsilon:

        for xc in bounds_iter:
            eps = 1.0E-09 * (xc - x_left) / points_in_interval

            x = np.linspace(x_left+eps, xc-eps, points_in_interval, endpoint=True)
            epsilon = np.sqrt(x/mu(s))

            y = np.vectorize(w_func)(x, mu(s))

            x.tofile(f_x)
            epsilon.tofile(f_epsilon)
            y.tofile(f_dw)

            y_plt_min = min(y.min(), y_plt_min)
            y_plt_max = max(y.max(), y_plt_max)

            # ax1.plot(x, y, 'b-')
            ax1.plot(epsilon, y, 'b-')

            # lambda:
            y = K_lambda/y

            y.tofile(f_lambda)

            yl_plt_min = min(y.min(), yl_plt_min)
            yl_plt_max = max(y.max(), yl_plt_max)

            # ax2.plot(x, y, 'b-')
            ax2.plot(epsilon, y, 'b-')

            x_left = xc

    print("decay width (sum) was plotted in {} seconds".format(next(_timing)))

    # ==============================================================
    # plot line by integration the expression from paper [KuznMikhSergh] (1)
    filenames = {
        'decay_width': 'decay_width_i-s_{:.05f}-epsmax_{}-p_{}{}.bin'.format(
            s, _x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
        'lambda': 'lambda_i-s_{:.05f}-epsmax_{}-p_{}{}.bin'.format(
            s, _x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
        'x': 'x_i-s_{:.05f}-epsmax_{}-p_{}{}.bin'.format(
            s, _x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
        'epsilon': 'epsilon_i-s_{:.05f}-epsmax_{}-p_{}{}.bin'.format(
            s, _x_max, points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        ),
    }

    with open(filenames['decay_width'], 'wb') as f_dw, \
         open(filenames['lambda'], 'wb') as f_lambda, \
         open(filenames['x'], 'wb') as f_x, \
         open(filenames['epsilon'], 'wb') as f_epsilon:

        x = np.linspace(x_min, x_max, points_in_interval, endpoint=True)
        epsilon = np.sqrt(x/mu(s))
        y = np.vectorize(wPhi_log)(x, mu(s))
        x.tofile(f_x)
        epsilon.tofile(f_epsilon)
        y.tofile(f_dw)

        y_plt_min = min(y.min(), y_plt_min)
        y_plt_max = max(y.max(), y_plt_max)
        ax1.plot(epsilon, y, 'r-')

        # lambda:
        y = K_lambda/y
        y.tofile(f_lambda)

        yl_plt_min = min(y.min(), yl_plt_min)
        yl_plt_max = max(y.max(), yl_plt_max)
        ax2.plot(epsilon, y, 'r-')

        # begin from zero
        x = np.linspace(1.0E-06, x_min, points_in_interval, endpoint=True)
        epsilon = np.sqrt(x/mu(s))
        y = np.vectorize(wPhi_log)(x, mu(s))
        x.tofile(f_x)
        epsilon.tofile(f_epsilon)
        y.tofile(f_dw)

        ax1.plot(epsilon, y, 'r--')

        # lambda:
        y = K_lambda/y
        y.tofile(f_lambda)

        ax2.plot(epsilon, y, 'r--')

    print("decay width (integral 1) was plotted in {} seconds".format(next(_timing)))
    x_min = 0.0
    # ==============================================================

    if logscale:
        # ax1.set_xscale('log')
        ax1.set_yscale('log')
        # ax2.set_xscale('log')
        ax2.set_yscale('log')

    if y_lim is None:
        y_plt_min = math.floor(y_plt_min * math.pow(10, -math.floor(math.log10(y_plt_min)))) * math.pow(10, math.floor(math.log10(y_plt_min)))
        y_plt_max = math.ceil(y_plt_max * math.pow(10, -math.floor(math.log10(y_plt_max)))) * math.pow(10, math.floor(math.log10(y_plt_max)))

    if yl_lim is None:
        yl_plt_min = math.floor(yl_plt_min * math.pow(10, -math.floor(math.log10(yl_plt_min)))) * math.pow(10, math.floor(math.log10(yl_plt_min)))
        yl_plt_max = math.ceil(yl_plt_max * math.pow(10, -math.floor(math.log10(yl_plt_max)))) * math.pow(10, math.floor(math.log10(yl_plt_max)))

    # ax1.set_xlim([x_min, x_max])
    # ax2.set_xlim([x_min, x_max])
    ax1.set_xlim([x_min, _x_max])
    ax2.set_xlim([x_min, _x_max])

    ax1.set_ylim(y_lim) if y_lim else ax1.set_ylim([y_plt_min, y_plt_max])
    ax2.set_ylim(yl_lim) if yl_lim else ax2.set_ylim([yl_plt_min, yl_plt_max])

    ax1.grid()
    ax2.grid()

    # plt.legend()
    fig1.savefig("decay_width-{}-s_{:.05f}-epsmax_{}-p_{}{}.png"
        .format(
            ('linscale', 'logscale')[logscale],
            s,
            _x_max,
            points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        )
    )
    fig2.savefig("lambda-{}-s_{:.05f}-epsmax_{}-p_{}{}.png"
        .format(
            ('linscale', 'logscale')[logscale],
            s,
            x_max,
            points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        )
    )
    print("image saved in {} seconds".format(next(_timing)))


def plt_all_energy_lambda(
        s_x_max_list,
        points_in_interval=2000,
        logscale=True,
        y_lim=None,
        yl_lim=None,
        filename_postfix='',
        w_func=wp_nm,
    ):
    """
    """
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()

    _timing = timing()
    next(_timing)

    remark = 'logarithmic scale' if logscale else 'linear scale'

    ax1.set_title('Decay width $w(x)$ for different $\\beta$; {}'.format(remark))
    ax1.set_xlabel('Transversal momentum, $x = p_\\perp^2/(2 \\beta)$')
    ax1.set_ylabel('Decay width $w(x)$, [GeV]')

    ax2.set_title('Mean free path $\\lambda$ for different $\\beta$; {}'.format(remark))
    ax2.set_xlabel('Transversal momentum, $x = p_\\perp^2/(2 \\beta)$')
    ax2.set_ylabel('$\\lambda$, [cm]')

    cc = (
        '#3333ff',
        '#33ff33',
        '#ff3333',
        '#aa00aa',
        '#00aaaa',
        '#aaaa00',
        '#888888',
        '#000000',
    )
    # ax1.set_prop_cycle(cycler(cc))
    # ax2.set_prop_cycle(cycler(cc))
    color_iter = iter(cc)

    ax1_lines = []
    ax2_lines = []

    x_min = float('inf')
    x_max = float('-inf')

    for (s, _x_max) in s_x_max_list:
        x_min = min(x_min, mu(s) - 0.5)
        x_max = max(x_max, _x_max)

        bounds = list(set([x for x in bounds_gen(_x_max, mu(s))]))
        bounds.sort()
        bounds_iter = iter(bounds)

        x_left = next(bounds_iter)

        y_plt_min = float('inf')
        y_plt_max = float('-inf')
        yl_plt_min = float('inf')
        yl_plt_max = float('-inf')

        color = next(color_iter)

        first_bound = True
        for xc in bounds_iter:
            eps = 1.0E-09 * (xc - x_left) / points_in_interval

            x = np.linspace(x_left+eps, xc-eps, points_in_interval)
            y = np.vectorize(w_func)(x, mu(s))

            if first_bound:
                rline1, = ax1.plot(x, y, '-', color=color, label="$\\beta = {}\\,m_W^2$".format(s), )
            else:
                ax1.plot(x, y, '-', color=color, )

            y_plt_min = min(y.min(), y_plt_min)
            y_plt_max = max(y.max(), y_plt_max)

            # lambda:
            y = K_lambda/y

            yl_plt_min = min(y.min(), yl_plt_min)
            yl_plt_max = max(y.max(), yl_plt_max)

            if first_bound:
                rline2, = ax2.plot(x, y, '-', color=color, label="$\\beta = {}\\,m_W^2$".format(s), )
            else:
                ax2.plot(x, y, '-', color=color, )

            x_left = xc
            first_bound = False

        ax1_lines.append(rline1)
        ax2_lines.append(rline2)

    print("all decay widths (sum) was plotted in {} seconds".format(next(_timing)))

    if logscale:
        # ax1.set_xscale('log')
        ax1.set_yscale('log')
        # ax2.set_xscale('log')
        ax2.set_yscale('log')

    y_plt_min = math.floor(y_plt_min * math.pow(10, -math.floor(math.log10(y_plt_min)))) * math.pow(10, math.floor(math.log10(y_plt_min)))
    y_plt_max = math.ceil(y_plt_max * math.pow(10, -math.floor(math.log10(y_plt_max)))) * math.pow(10, math.floor(math.log10(y_plt_max)))
    yl_plt_min = math.floor(yl_plt_min * math.pow(10, -math.floor(math.log10(yl_plt_min)))) * math.pow(10, math.floor(math.log10(yl_plt_min)))
    yl_plt_max = math.ceil(yl_plt_max * math.pow(10, -math.floor(math.log10(yl_plt_max)))) * math.pow(10, math.floor(math.log10(yl_plt_max)))

    ax1.set_xlim([x_min, x_max])
    ax2.set_xlim([x_min, x_max])

    ax1.set_ylim(y_lim) if y_lim else ax1.set_ylim([y_plt_min, y_plt_max])
    ax2.set_ylim(yl_lim) if yl_lim else ax2.set_ylim([yl_plt_min, yl_plt_max])

    ax1.grid()
    ax2.grid()

    ax1.legend(loc='best')
    ax2.legend(loc='best')
    # ax1.legend(ax1_lines, labels, loc='best')
    # ax2.legend(ax2_lines, labels, loc='best')

    fig1.savefig("decay_width-{}-s_all-xmax_{}-p_{}{}.png"
        .format(
            ('linscale', 'logscale')[logscale],
            x_max,
            points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        )
    )
    fig2.savefig("lambda-{}-s_all-xmax_{}-p_{}{}.png"
        .format(
            ('linscale', 'logscale')[logscale],
            x_max,
            points_in_interval,
            '-'+filename_postfix if filename_postfix else filename_postfix,
        )
    )
    print("image saved in {} seconds".format(next(_timing)))


plots_log = ( # total: y_lim (1.0E-24, 2.0E+00); yl_lim (1.0E-14, 2.0E+10)
    # {'s': 1./100., 'x_max': 100.0, 'logscale': True, 'y_lim': (1.0E-24, 1.0E-09), 'yl_lim': (5.0E-05, 2.0E+10)},
    # {'s': 1./80. , 'x_max': 090.0, 'logscale': True, 'y_lim': (2.0E-20, 1.0E-07), 'yl_lim': (1.0E-07, 7.0E+05)},
    # {'s': 1./50. , 'x_max': 070.0, 'logscale': True, 'y_lim': (2.0E-13, 3.0E-05), 'yl_lim': (4.0E-10, 7.0E-02)},
    # {'s': 0.030  ,  x_max': 050.0, 'logscale': True, 'y_lim': (1.0E-09, 5.0E-04), 'yl_lim': (3.0E-11, 2.0E-05)},
    # {'s': 1./30. , 'x_max': 050.0, 'logscale': True, 'y_lim': (1.0E-08, 1.0E-03), 'yl_lim': (1.0E-11, 1.0E-05)},
    # {'s': 1./20. , 'x_max': 045.0, 'logscale': True, 'y_lim': (1.0E-06, 6.0E-03), 'yl_lim': (3.0E-12, 2.0E-08)},
    # {'s': 1./10. , 'x_max': 030.0, 'logscale': True, 'y_lim': (5.0E-04, 4.0E-02), 'yl_lim': (5.0E-13, 3.0E-11)},
    # {'s': 1./3.  , 'x_max': 050.0, 'logscale': True, 'y_lim': (5.0E-02, 2.0E+00), 'yl_lim': (1.0E-14, 4.0E-13)},
    # #
    {'s': 1./100., 'x_max': 5.0, 'logscale': True, 'y_lim': (1.0E-24, 1.0E-09), 'yl_lim': (5.0E-05, 2.0E+10)},
    {'s': 1./80. , 'x_max': 5.0, 'logscale': True, 'y_lim': (2.0E-20, 1.0E-07), 'yl_lim': (1.0E-07, 7.0E+05)},
    {'s': 1./50. , 'x_max': 5.0, 'logscale': True, 'y_lim': (2.0E-13, 3.0E-05), 'yl_lim': (4.0E-10, 7.0E-02)},
    {'s': 0.030  , 'x_max': 5.0, 'logscale': True, 'y_lim': (1.0E-09, 5.0E-04), 'yl_lim': (3.0E-11, 2.0E-05)},
    {'s': 1./30. , 'x_max': 5.0, 'logscale': True, 'y_lim': (1.0E-08, 1.0E-03), 'yl_lim': (1.0E-11, 1.0E-05)},
    {'s': 1./20. , 'x_max': 5.0, 'logscale': True, 'y_lim': (1.0E-06, 6.0E-03), 'yl_lim': (3.0E-12, 2.0E-08)},
    {'s': 1./10. , 'x_max': 5.0, 'logscale': True, 'y_lim': (5.0E-04, 4.0E-02), 'yl_lim': (5.0E-13, 3.0E-11)},
    {'s': 1./3.  , 'x_max': 5.0, 'logscale': True, 'y_lim': (5.0E-02, 2.0E+00), 'yl_lim': (1.0E-14, 4.0E-13)},
    {'s': 1./2.  , 'x_max': 5.0, 'logscale': True, 'y_lim': (5.0E-02, 2.0E+00), 'yl_lim': (1.0E-14, 4.0E-13)},
    #
    {'s': 3./5.  , 'x_max': 5.0, 'logscale': True, 'y_lim': (5.0E-02, 2.0E+00), 'yl_lim': (1.0E-14, 4.0E-13)},
    {'s': 7./10. , 'x_max': 5.0, 'logscale': True, 'y_lim': (5.0E-02, 2.0E+00), 'yl_lim': (1.0E-14, 4.0E-13)},
    {'s': 4./5.  , 'x_max': 5.0, 'logscale': True, 'y_lim': (5.0E-02, 2.0E+00), 'yl_lim': (1.0E-14, 4.0E-13)},
    {'s': 9./10. , 'x_max': 5.0, 'logscale': True, 'y_lim': (5.0E-02, 2.0E+00), 'yl_lim': (1.0E-14, 4.0E-13)},
    #
    # {'s': 1./100., 'x_max': 100.0, 'logscale': True, 'y_lim': (1.0E-24, 1.0E-09), 'yl_lim': (5.0E-05, 2.0E+10)},
    # {'s': 1./80. , 'x_max': 100.0, 'logscale': True, 'y_lim': (2.0E-20, 1.0E-07), 'yl_lim': (1.0E-07, 7.0E+05)},
    # {'s': 1./50. , 'x_max': 100.0, 'logscale': True, 'y_lim': (2.0E-13, 3.0E-05), 'yl_lim': (4.0E-10, 7.0E-02)},
    # {'s': 0.030  , 'x_max': 100.0, 'logscale': True, 'y_lim': (1.0E-09, 5.0E-04), 'yl_lim': (3.0E-11, 2.0E-05)},
    # {'s': 1./30. , 'x_max': 100.0, 'logscale': True, 'y_lim': (1.0E-08, 1.0E-03), 'yl_lim': (1.0E-11, 1.0E-05)},
    # {'s': 1./20. , 'x_max': 100.0, 'logscale': True, 'y_lim': (1.0E-06, 6.0E-03), 'yl_lim': (3.0E-12, 2.0E-08)},
    # {'s': 1./10. , 'x_max': 100.0, 'logscale': True, 'y_lim': (5.0E-04, 4.0E-02), 'yl_lim': (5.0E-13, 3.0E-11)},
    # {'s': 1./3.  , 'x_max': 100.0, 'logscale': True, 'y_lim': (5.0E-02, 2.0E+00), 'yl_lim': (1.0E-14, 4.0E-13)},
    # {'s': 1./2.  , 'x_max': 100.0, 'logscale': True, 'y_lim': (5.0E-02, 2.0E+00), 'yl_lim': (1.0E-14, 4.0E-13)},
)

plots_lin = ( # total: y_lim (0, 2.0E+00); yl_lim (0, 2.0E+10)
    {'s': 1./100., 'x_max': 100.0, 'logscale': False, 'y_lim': (0.0, 1.0E-09), 'yl_lim': (0.0, 2.0E+10)},
    {'s': 1./80. , 'x_max': 090.0, 'logscale': False, 'y_lim': (0.0, 1.0E-07), 'yl_lim': (0.0, 7.0E+05)},
    {'s': 1./50. , 'x_max': 070.0, 'logscale': False, 'y_lim': (0.0, 3.0E-05), 'yl_lim': (0.0, 7.0E-02)},
    {'s': 0.030  , 'x_max': 050.0, 'logscale': False, 'y_lim': (0.0, 5.0E-04), 'yl_lim': (0.0, 2.0E-05)},
    {'s': 1./30. , 'x_max': 050.0, 'logscale': False, 'y_lim': (0.0, 1.0E-03), 'yl_lim': (0.0, 1.0E-05)},
    {'s': 1./20. , 'x_max': 045.0, 'logscale': False, 'y_lim': (0.0, 6.0E-03), 'yl_lim': (0.0, 2.0E-08)},
    {'s': 1./10. , 'x_max': 030.0, 'logscale': False, 'y_lim': (0.0, 4.0E-02), 'yl_lim': (0.0, 3.0E-11)},
    {'s': 1./3.  , 'x_max': 050.0, 'logscale': False, 'y_lim': (0.0, 2.0E+00), 'yl_lim': (0.0, 4.0E-13)},
    {'s': 1./2.  , 'x_max': 050.0, 'logscale': False, 'y_lim': (0.0, 2.0E+00), 'yl_lim': (0.0, 4.0E-13)},
    {'s': 3./5.  , 'x_max': 050.0, 'logscale': False, 'y_lim': (0.0, 2.0E+00), 'yl_lim': (0.0, 4.0E-13)},
    {'s': 7./10. , 'x_max': 050.0, 'logscale': False, 'y_lim': (0.0, 2.0E+00), 'yl_lim': (0.0, 4.0E-13)},
    {'s': 4./5.  , 'x_max': 050.0, 'logscale': False, 'y_lim': (0.0, 2.0E+00), 'yl_lim': (0.0, 4.0E-13)},
    {'s': 9./10. , 'x_max': 050.0, 'logscale': False, 'y_lim': (0.0, 2.0E+00), 'yl_lim': (0.0, 4.0E-13)},
)

s_x_max_tuples = (
    # (1/100 , 100.0, ),
    # (1/80  , 090.0, ),
    # (1/50  , 070.0, ),
    # (0.0300, 050.0, ),
    # (1/30  , 050.0, ),
    # (1/20  , 045.0, ),
    # (1/10  , 030.0, ),
    # (1/3   , 050.0, ),
    # (1/2   , 050.0, ),

    (1./100. , 100.0, ),
    (1./80.  , 100.0, ),
    (1./50.  , 100.0, ),
    (0.0300  , 100.0, ),
    (1./30.  , 100.0, ),
    (1./20.  , 100.0, ),
    (1./10.  , 100.0, ),
    (1./3.   , 100.0, ),
    (1./2.   , 100.0, ),
    (3./5.   , 100.0, ),
    (7./10.  , 100.0, ),
    (4./5.   , 100.0, ),
    (9./10.  , 100.0, ),
)

s_epsilon_max_tuples = (
    (1./100. , 5.0, ),
    (1./80.  , 5.0, ),
    (1./50.  , 5.0, ),
    (0.0300  , 5.0, ),
    (1./30.  , 5.0, ),
    (1./20.  , 5.0, ),
    (1./10.  , 5.0, ),
    (1./3.   , 5.0, ),
    (1./2.   , 5.0, ),
    (3./5.   , 5.0, ),
    (7./10.  , 5.0, ),
    (4./5.   , 5.0, ),
    (9./10.  , 5.0, ),
)

def __main__():
    # s_epsilons = (
    #     # (s,       epsilon),
    #     ## m_W^2/e, m_W^2/e
    #     (0.1, 1.0),
    #     (0.1, 1.5),
    #     (0.1, 2.0-1.0E-12),
    #     (0.1, 2.0),
    #     (0.1, 2.0+1.0E-12),

    #     (0.5, 1.0),
    #     (0.5, 1.5),
    #     (0.5, 2.0-1.0E-12),
    #     (0.5, 2.0),
    #     (0.5, 2.0+1.0E-12),

    #     (1.0-1.0E-16, 1.0),
    #     (1.0-1.0E-16, 1.5),
    #     (1.0-1.0E-16, 2.0-1.0E-12),
    #     (1.0-1.0E-16, 2.0),
    #     (1.0-1.0E-16, 2.0+1.0E-12),

    #     (1.0-1.0E-12, 1.0),
    #     (1.0-1.0E-12, 1.5),
    #     (1.0-1.0E-12, 2.0-1.0E-12),
    #     (1.0-1.0E-12, 2.0),
    #     (1.0-1.0E-12, 2.0+1.0E-12),

    #     (1.0, 1.0),
    #     (1.0, 1.5),
    #     (1.0, 2.0-1.0E-12),
    #     (1.0, 2.0),
    #     (1.0, 2.0+1.0E-12),
    # )

    # for (s, epsilon) in s_epsilons:
    #     _mu = mu(s)
    #     x = _mu * epsilon * epsilon
    #     print("b = {b:.02g}; epsilon = {epsilon:.02g}; x = {x:.02g}".format(b=s, epsilon=epsilon, x=x))
    #     print("    wp = {:.05e} GeV".format(wp_nm(x, _mu)))
    #     print("    wm = {:.05e} GeV".format(wm_nm(x, _mu)))
    #     print("    lambda_p = {:.05e} cm".format(K_lambda/wp_nm(x, _mu)))
    #     print("    lambda_m = {:.05e} cm".format(K_lambda/wm_nm(x, _mu)))
    #     print()
    #     print("    wPhi = {:.05e} GeV".format(wPhi_log(x, _mu)))
    #     print("    lambdaPhi = {:.05e} cm".format(K_lambda/wPhi_log(x, _mu)))
    #     print(" =======================================")
    # exit()

    # args = parse_args()
    # plt_energy_lambda(
    #     s=args.field_strength,
    #     x_max=args.x_max,
    #     points_in_interval=args.points_in_interval,
    #     logscale=args.logscale,
    #     y_lim=None,
    #     yl_lim=None,
    #     filename_postfix='',
    # )

    import sys
    print("started!")
    # if len(sys.argv) > 1:
    if int(sys.argv[1]) <= 255:
        print(" -- single plot for single parameter")
        g = plots_log[int(sys.argv[1])]

        print('== Function name: {}_nm'.format(('wp', 'wm', 'wz')[int(sys.argv[2])]))
        # plt_energy_lambda(
        plt_eps_energy_lambda(
            s=g['s'],
            x_max=g['x_max'],
            points_in_interval=2000,
            # points_in_interval=200,
            # points_in_interval=50,
            # points_in_interval=int(sys.argv[2]),
            logscale=g['logscale'],
            # y_lim=g['y_lim'],
            # yl_lim=g['yl_lim'],
            # filename_postfix='',
            # w_func=wp_nm,
            # w_func=wm_nm,
            # w_func=wp_mn,
            # w_func=wm_mn,
            filename_postfix='w-integr-eps-{}-final'.format(('wp', 'wm', 'wz')[int(sys.argv[2])]),
            w_func=(wp_nm, wm_nm, wz_nm)[int(sys.argv[2])],
        )
        # plt_energy_lambda(
        #     s=g['s'],
        #     x_max=g['x_max'],
        #     points_in_interval=2000,
        #     logscale=g['logscale'],
        #     y_lim=g['y_lim'],
        #     yl_lim=g['yl_lim'],
        #     # filename_postfix='',
        #     filename_postfix='w-integr-100-final',
        #     w_func=wp_nm,
        #     # w_func=wm_nm,
        #     # w_func=wp_mn,
        #     # w_func=wm_mn,
        # )

    # for g in plots_log:
    #     plt_energy_lambda(
    #         s=g['s'],
    #         x_max=g['x_max'],
    #         points_in_interval=2000,
    #         logscale=g['logscale'],
    #         y_lim=g['y_lim'],
    #         yl_lim=g['yl_lim'],
    #         filename_postfix='',
    #         w_func=wp_nm,
    #     )

    # for g in plots_lin:
    #     plt_energy_lambda(
    #         s=g['s'],
    #         x_max=g['x_max'],
    #         points_in_interval=2000,
    #         logscale=g['logscale'],
    #         y_lim=g['y_lim'],
    #         yl_lim=g['yl_lim'],
    #         filename_postfix='',
    #         w_func=wp_nm,
    #     )

    # # # # # # # # # # # # # # # # # #
    # plt_all_energy_lambda(
    #     s_x_max_list=s_x_max_tuples,
    #     points_in_interval=200,
    #     logscale=True,
    #     y_lim=(1.0E-24, 2.0E+00),
    #     yl_lim=(1.0E-14, 2.0E+10),
    #     filename_postfix='',
    #     w_func=wp_nm,
    # )

    # plt_all_energy_lambda(
    #     s_x_max_list=s_x_max_tuples,
    #     points_in_interval=200,
    #     logscale=False,
    #     y_lim=(0, 2.0E+00),
    #     yl_lim=(0, 2.0E+10),
    #     filename_postfix='',
    #     w_func=wp_nm,
    # )

if __name__ == '__main__':
    __main__()
