inkscape\inkscape.exe %1 -E %~n1.eps --export-ignore-filters --export-ps-level=3
inkscape\inkscape.exe %1 --export-pdf=%~n1.svg.pdf --export-area-drawing --without-gui --export-ignore-filters --export-ps-level=3
