#!/usr/bin/env python3
from math import exp, sqrt, pi
import numpy as np

m_e = 0.5109989461E-03 # electron mass in GeV/c^2
m_W = 80.385          # W-boson mass in GeV/c^2
G_F = 1.16637E-05     # Fermi coupling constant in GeV^{-2}
K_lambda = 6.582119514E-16 * 2.99792458E+01                       # [GeV cm] the factor to convert GeV^{-1} to cm

data = [
    {'dirname': "s_0.01000-epsmax_5.0-p_200-w-integr-wm-final",  's': 0.01000, },
    {'dirname': "s_0.01000-epsmax_5.0-p_50-w-integr-wm-final",   's': 0.01000, },
    {'dirname': "s_0.01250-epsmax_5.0-p_200-w-integr-wm-final",  's': 0.01250, },
    {'dirname': "s_0.01250-epsmax_5.0-p_50-w-integr-wm-final",   's': 0.01250, },
    {'dirname': "s_0.02000-epsmax_5.0-p_200-w-integr-wm-final",  's': 0.02000, },
    {'dirname': "s_0.02000-epsmax_5.0-p_50-w-integr-wm-final",   's': 0.02000, },
    {'dirname': "s_0.03000-epsmax_5.0-p_200-w-integr-wm-final",  's': 0.03000, },
    {'dirname': "s_0.03000-epsmax_5.0-p_50-w-integr-wm-final",   's': 0.03000, },
    {'dirname': "s_0.03333-epsmax_5.0-p_200-w-integr-wm-final",  's': 1/30.0,  },
    {'dirname': "s_0.05000-epsmax_5.0-p_200-w-integr-wm-final",  's': 0.05000, },
    {'dirname': "s_0.10000-epsmax_5.0-p_200-w-integr-wm-final",  's': 0.10000, },
    {'dirname': "s_0.33333-epsmax_5.0-p_200-w-integr-wm-final",  's': 1/3.0,   },
    {'dirname': "s_0.33333-xmax_100.0-p_200-w-integr-wm-final",  's': 1/3.0,   },
    {'dirname': "s_0.50000-epsmax_5.0-p_200-w-integr-wm-final",  's': 0.50000, },
    {'dirname': "s_0.50000-xmax_100.0-p_200-w-integr-wm-final",  's': 0.50000, },
    {'dirname': "s_0.60000-epsmax_5.0-p_2000-w-integr-wm-final", 's': 0.60000, },
    {'dirname': "s_0.70000-epsmax_5.0-p_2000-w-integr-wm-final", 's': 0.70000, },
    {'dirname': "s_0.80000-epsmax_5.0-p_2000-w-integr-wm-final", 's': 0.80000, },
    {'dirname': "s_0.90000-epsmax_5.0-p_2000-w-integr-wm-final", 's': 0.90000, },
]

def w_min(s):
    return G_F * m_W * m_W * m_W * s * exp(- (1 - s) / (2.0 * s)) / (pi * sqrt(2) * sqrt(1-s))

def x_min(s):
    return 1.0/(2.0 * s) - 0.5

def epsilon_min(s):
    return sqrt(1.0 - s)

def __main__():
    for g in data:
        x = x_min(g['s'])
        x = np.array([x, x])
        x.tofile('_bin/{}/x_z.bin'.format(g['dirname']))

        epsilon = epsilon_min(g['s'])
        epsilon = np.array([epsilon, epsilon])
        epsilon.tofile('_bin/{}/epsilon_z.bin'.format(g['dirname']))

        w = w_min(g['s'])
        w = np.array([0.0, w])
        w.tofile('_bin/{}/decay_width_z.bin'.format(g['dirname']))

        l = K_lambda/w_min(g['s'])
        # l = np.array([float('inf'), l])
        l = np.linspace(20.0, 1.0, 21)*l
        l.tofile('_bin/{}/lambda_z.bin'.format(g['dirname']))
        np.array([x_min(g['s'])]*21).tofile('_bin/{}/x_lambda_z.bin'.format(g['dirname']))
        np.array([epsilon_min(g['s'])]*21).tofile('_bin/{}/epsilon_lambda_z.bin'.format(g['dirname']))

if __name__ == '__main__':
    __main__()
