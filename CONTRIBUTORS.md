CONTRIBUTORS \ CONTRIBUTORS \ CONTRIBUTORS
==========================================

* **[Alexander Kuznetsov](http://)**

  * Analytical calcultations, leadership

* **[Alexander Okrugin](https://bitbucket.org/aviafelix/)**

  * First code, analytical calcultations

* **[Anastasija Shitova](http://)**

  * Analytical calcultations
