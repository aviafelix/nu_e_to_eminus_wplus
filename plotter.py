#!/usr/bin/env python3
import os
import numpy as np
import matplotlib.pyplot as plt
# import matplotlib
# matplotlib.use('Agg')
import plotsettings
# import argparse
import json5 as json

CONFIG_FILE = 'plots.json5'
DATA_KEY = '__DATA__'
PLOTS_KEY = '__PLOTS__'
LINKS_KEY = '__LINKS__'
INPUT_PATH_KEY = '__PATH_INPUT__'
OUTPUT_PATH_KEY = '__PATH_OUTPUT__'
SUBSTITUTIONS_KEY = '__SUBST__'

def load_config(config_file=CONFIG_FILE):
    print("config file name: `{}`".format(config_file))

    with open(config_file, 'r') as f:
        try:
            return json.load(f)
        except Exception as e: # for json5
            print("error in config:", e)
            return None

def get_cfg_property(
        cfg,
        var_data=None,
        links=None,
    ):

    var_data_type = type(var_data)

    if (var_data_type is dict):
        if (links is not None):
            (link_key, _var_data), = var_data.items()
            if link_key in links.keys():
                return get_cfg_property(cfg=cfg, var_data=cfg[links[link_key]][_var_data], links=links)
            elif link_key in cfg.keys():
                return get_cfg_property(cfg=cfg, var_data=cfg[link_key][_var_data], links=links)
        if link_key == SUBSTITUTIONS_KEY:
            return var_data
    else:
        return var_data

def process_config(cfg=None):
    """
    returns processed config as (data, plots)
    """
    print("processing config...")

    data = cfg[DATA_KEY]
    plots = cfg[PLOTS_KEY]
    links = cfg[LINKS_KEY] if LINKS_KEY in cfg.keys() else None

    ret_data = {}

    for key in data.keys():
        ret_data[key] = get_cfg_property(
            cfg=cfg,
            var_data=data[key],
            links=links,
        )

    return (ret_data, plots)

def substitute_data(data, substitution):
    ret_data = {}

    for key in data.keys():
        if key != SUBSTITUTIONS_KEY:
            data_value = data[key]
            if (type(data_value) is dict) and (SUBSTITUTIONS_KEY in data_value.keys()):
                ret_data[key] = substitution[data_value[SUBSTITUTIONS_KEY]]
            elif (type(data_value) is str):
                ret_data[key] = data_value.format(**substitution)
            elif (type(data_value) is not dict):
                ret_data[key] = data_value
        else:
            ret_data[key] = substitution
    return ret_data

def make_plots(data, plots):
    os.makedirs(data[OUTPUT_PATH_KEY], exist_ok=True)

    cnt = 0
    if SUBSTITUTIONS_KEY in data.keys():
        for substitution in data[SUBSTITUTIONS_KEY]:
            subst_data = substitute_data(data=data, substitution=substitution)

            for plot in plots:
                print("=== [ making plot #{} ] ===".format(cnt))
                make_one_plot(data=subst_data, plot=plot)
                cnt += 1
    else:
        for plot in plots:
            print("=== [ making plot #{} ] ===".format(cnt))
            make_one_plot(data=data, plot=plot)
            cnt += 1

def multiply_list(factor, data):
    return list(map(lambda x: factor*x, data))

def make_one_plot(data, plot):
    fig, ax = plt.subplots()
    substitutions = data[SUBSTITUTIONS_KEY] if SUBSTITUTIONS_KEY in data.keys() else {}
    if 'title' in data.keys():
        ax.set_title(data['title'].format(**substitutions))
    elif 'title' in plot.keys():
        ax.set_title(plot['title'].format(**substitutions))

    label = {}
    if type(plot['x_label']) is dict:
        for key in plot['x_label']:
            if type(plot['x_label'][key]) == str:
                label[key] = plot['x_label'][key].format(**substitutions)
            else:
                label[key] = plot['x_label'][key]
        ax.set_xlabel(**label)
    else:
        ax.set_xlabel(plot['x_label'].format(**substitutions))

    label = {}
    if type(plot['y_label']) is dict:
        for key in plot['y_label']:
            if type(plot['y_label'][key]) == str:
                label[key] = plot['y_label'][key].format(**substitutions)
            else:
                label[key] = plot['y_label'][key]
        ax.set_ylabel(**label)
    else:
        ax.set_ylabel(plot['y_label'].format(**substitutions))

    x_factor = plot['x_factor'] if 'x_factor' in plot.keys() else 1.0
    y_factor = plot['y_factor'] if 'y_factor' in plot.keys() else 1.0

    if x_factor in data.keys():
        x_factor = data[x_factor]

    if y_factor in data.keys():
        y_factor = data[y_factor]

    print("Ok, making plot...")
    cnt = 0
    for graph in plot['graphs']:
        print("making graph #{}".format(cnt))
        cnt += 1

        plot_settings_list = graph['plot_settings_list'] if 'plot_settings_list' in graph.keys() else []
        plot_settings_dict = graph['plot_settings_dict'] if 'plot_settings_dict' in graph.keys() else {}

        print("reading data for x: `{}`...".format(data[INPUT_PATH_KEY]+data[graph['x_coordinate']]))
        x = np.fromfile(data[INPUT_PATH_KEY]+data[graph['x_coordinate']])
        print("reading data for y: `{}`...".format(data[INPUT_PATH_KEY]+data[graph['y_coordinate']]))
        y = np.fromfile(data[INPUT_PATH_KEY]+data[graph['y_coordinate']])

        # "linked" sorting; two parallel lists sort
        print("sorting...")
        p = np.argsort(x)
        x = x[p]
        y = y[p]
        xb = None
        bounds = None

        if 'bounds_coordinate' in graph.keys():
            print("bounds coordinate is read from `{}` and sorted".format(data[INPUT_PATH_KEY]+data[graph['bounds_coordinate']]))
            bounds_coordinate = np.fromfile(data[INPUT_PATH_KEY]+data[graph['bounds_coordinate']])[p]
        else:
            print("bounds coordinate is setted to `x`")
            bounds_coordinate = x

        if 'bounds' in graph.keys():
            print("reading bounds data... ", end="")
            if type(graph['bounds']) is str:
                bounds = data[graph['bounds']]
                if type(bounds) is str:
                    bounds = np.fromfile(data[INPUT_PATH_KEY]+data[graph['bounds']])
                elif type(bounds) is not list:
                    bounds = None
            print("ok")

            if bounds is not None:
                print("bounds length:", len(bounds))
                print("plotting data with bounds:")

                print("adding bounds to data...", end=" ")
                xb = np.append(bounds_coordinate, bounds)
                x = np.append(x, np.full(len(bounds), np.nan))
                y = np.append(y, np.full(len(bounds), np.nan))
                p = np.argsort(xb)
                x = x[p]
                y = y[p]
                xb = xb[p]
                print("Ok!")

        x_min = graph['x_min'] if 'x_min' in graph.keys() else -np.inf
        x_max = graph['x_max'] if 'x_max' in graph.keys() else np.inf
        y_min = graph['y_min'] if 'y_min' in graph.keys() else -np.inf
        y_max = graph['y_max'] if 'y_max' in graph.keys() else np.inf

        if x_min in data.keys():
            x_min = data[x_min]
        if x_max in data.keys():
            x_max = data[x_max]
        if y_min in data.keys():
            y_min = data[y_min]
        if y_max in data.keys():
            y_max = data[y_max]

        if 'x_minmax_bind' in graph.keys():
            x_minmax_bind = graph['x_minmax_bind']
            if x_minmax_bind == 'x_coordinate':
                print("x_bound is binded to `x_coordinate`")
                xb = x
            if x_minmax_bind == 'bounds_coordinate':
                print("x_bound is binded to `bounds_coordinate`")
                xb = bounds_coordinate

        if xb is None:
            print("x_bound is setted to `bounds_coordinate` (was `None`)")
            xb = bounds_coordinate

        cond = np.logical_or(
            np.isnan(x),
            np.logical_and(xb >= x_min, xb <= x_max),
        )
        x = x[cond]
        y = y[cond]

        cond = np.logical_or(
            np.isnan(y),
            np.logical_and(y >= y_min, y <= y_max),
        )
        x = x[cond]
        y = y[cond]

        ax.plot(x_factor*x, y_factor*y, *plot_settings_list, **plot_settings_dict)

    x_lim = plot['x_lim'] if 'x_lim' in plot.keys() else None
    y_lim = plot['y_lim'] if 'y_lim' in plot.keys() else None

    if x_lim in data.keys():
        x_lim = data[x_lim]

    if y_lim in data.keys():
        y_lim = data[y_lim]

    ax.set_xlim(multiply_list(x_factor, x_lim)) if x_lim else None
    ax.set_ylim(multiply_list(y_factor, y_lim)) if y_lim else None
    # ax.set_xlim(multiply_list(x_factor, x_lim)) if x_lim else ax.set_xlim(multiply_list(x_factor, [x_plt_min, x_plt_max]))
    # ax.set_ylim(multiply_list(y_factor, y_lim)) if y_lim else ax.set_ylim(multiply_list(y_factor, [y_plt_min, y_plt_max]))

    if 'x_scale' in plot.keys():
        x_scale = plot['x_scale']
        if x_scale in data.keys():
            x_scale = data[x_scale]
        ax.set_xscale(x_scale)

    if 'y_scale' in plot.keys():
        y_scale = plot['y_scale']
        if y_scale in data.keys():
            y_scale = data[y_scale]
        ax.set_yscale(y_scale)

    ax.grid()
    if 'legend' in plot.keys():
        ax.legend(**plot['legend'])
    print("saving figure to file", end="")
    filename = plot['filename']
    if filename in data.keys():
        filename = data[filename]
    print(' (`{}`)... '.format(data[OUTPUT_PATH_KEY]+filename), end="")
    fig.savefig(data[OUTPUT_PATH_KEY]+filename)
    plt.close(fig)
    print("Ok")

def main():
    import sys
    print("reading config:", end=', ')
    if len(sys.argv) > 1:
        cfg = load_config(config_file=sys.argv[1])
    else:
        cfg = load_config()
    if cfg is None:
        print("no config \ error in config: exiting")
        exit()

    (data, plots, ) = process_config(cfg=cfg)

    print("do make plots.")
    # Assume that input data is not described
    # by list (only strings of link to another data)
    make_plots(
        data=data,
        plots=plots,
    )

if __name__ == '__main__':
    main()
